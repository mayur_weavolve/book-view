<?php

use Illuminate\Support\Facades\Route;


Auth::routes();
Route::get('/logout', 'HomeController@logout')->name('logout');
Route::get('/privacypolicy', 'OpenController@privacypolicy')->name('privacypolicy');
Route::get('/termsofuse', 'OpenController@termsofuse')->name('termsofuse');
Route::group(['middleware' => 'auth'], function(){
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('welcome','HomeController@index')->name('welcome');

    Route::get('category','CategoryController@category')->name('category');
    Route::get('category','CategoryController@serach')->name('category');
    Route::get('category/add','CategoryController@add_category')->name('add_category');
    Route::post('category/add','CategoryController@save_category');
    Route::get('category/delete/{id}', 'CategoryController@delete_category')->name('delete_category');
    Route::get('category/update/{id}','CategoryController@update_category')->name('update_category');
    Route::post('category/update/{id}','CategoryController@updatesave_category');
    Route::get('category/export','CategoryController@exportCategory')->name('category_export');

    Route::get('teacher','AdminController@teacher')->name('teacher');
    Route::get('teacher','AdminController@serach')->name('teacher');
    Route::get('teacher/add','AdminController@add_teacher')->name('add_teacher');
    Route::post('teacher/add','AdminController@save_teacher');
    Route::get('teacher/delete/{id}', 'AdminController@delete_teacher')->name('delete_teacher');
    Route::get('teacher/update/{id}','AdminController@update_teacher')->name('update_teacher');
    Route::post('teacher/update/{id}','AdminController@updatesave_teacher');
    Route::get('teacher/export','AdminController@exportTeacher')->name('teacher_export');
    Route::get('teacher/terms-accept','AdminController@acceptTerms')->name('accept_terms');

    Route::get('student','TeacherController@student')->name('student');
    //Route::get('student','TeacherController@serach')->name('student');
    Route::get('student/add','TeacherController@add_student')->name('add_student');
    Route::post('student/add','TeacherController@save_student');
    Route::get('student/delete/{id}', 'TeacherController@delete_student')->name('delete_student');
    Route::get('student/update/{id}','TeacherController@update_student')->name('update_student');
    Route::post('student/update/{id}','TeacherController@updatesave_student');
    Route::get('student/export','TeacherController@exportStudent')->name('student_export');

    Route::get('book','BookController@book')->name('book');
    Route::get('book','BookController@serach')->name('book');
    Route::get('book/add','BookController@add_book')->name('add_book');
    Route::get('book/detail/{id}','BookController@book_detail')->name('book_detail');
    Route::post('book/add','BookController@save_book');
    Route::get('book/book-view/{id}','BookController@book_view')->name('book_view');
    Route::get('book/admin-bookview/{id}','BookController@admin_bookview')->name('admin_bookview');
    Route::get('book/delete/{id}', 'BookController@delete_book')->name('delete_book');
    Route::get('book/edit/{id}', 'BookController@book_edit')->name('edit_book');
    Route::post('book/edit/{id}', 'BookController@book_editsave');
    Route::get('book/export','BookController@exportBook')->name('book_export');

    Route::get('change-password','AdminController@change_password')->name('change-password');
    Route::post('change-password','AdminController@save_password');

    Route::get('admin_profile','AdminController@profile')->name('admin_profile');
    Route::post('admin_profile','AdminController@save_profile');

//------------------------------------User Action------------------------------------------ 

    // Route::get('category-list','UserController@category_list')->name('category_list');
    //Route::get('book-list/{id}','UserController@book_list')->name('book_list');

});
