<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
class TeamController extends Controller
{
    //

    public function index(){
        return view("welcome");
    }
    
    public function saveTeam(Request $request){
        $data = $request->all();
        $team  = new Team();
        $team->name = $data['name'];
        $team->team_id = $data['team_id'];
        $team->room_type = $data['room_type'];
        $team->password = ""; //$data['password'];
        $team->save();

        $request->session()->put('name', $team->name);
        $request->session()->put('team_id', $team->team_id);
        $request->session()->put('team_created', "yes");
        $request->session()->put('room_type', $team->room_type);
        return redirect("/join/" . $team->team_id);
    }
}
