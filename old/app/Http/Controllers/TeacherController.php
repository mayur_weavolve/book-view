<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\User;
use Validator;

class TeacherController extends Controller
{
    public function student()
    {
        if(Auth::user()->role_type == 1)
        {
            $list=User::with('teacher')->where('role_type',3)->orderBy('created_at','DESC')->get();
        }
        else{
            $teacher = Auth::user();
            $list=User::with('teacher')->where('role_type',3)->where('is_deleted',0)->where('perent_id',$teacher->id)->orderBy('created_at','DESC')->get();
        }
        $data=[];
        $data['users']=$list;
        return view('student.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        if(Auth::user()->role_type == 1)
        {
            $list=User::with('teacher')
                        ->where('name','like',"%{$serach}%")
                        // ->where('email','like',$serach.'%')
                        ->where('role_type',3)
                        ->orderBy('created_at')
                        ->paginate(10);
        }
        else{
            $teacher = Auth::user();
            $list=User::with('teacher')
                        ->where('name','like',"%{$serach}%")
                        // ->where('email','like',$serach.'%')
                        ->where('role_type',3)
                        ->where('is_deleted',0)
                        ->where('perent_id',$teacher->id)
                        ->orderBy('created_at')
                        ->paginate(10);
        }
        $data=[];
        $data['users']=$list;
        return view('student.list',$data);
    }
    public function add_student()
    {
        $teacher = Auth::user();
        return view('student.add');
    }
    public function save_student(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|unique:users',  
            'password' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_student'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $users = new User();
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = Hash::make($data['password']);
        $users->perent_id = $teacher->id;
        $users->status = "active";
        $users->role_type = 3 ;
        $users->is_deleted = 0;
        $users->save();
        if($teacher->remain_licence_count == ''){
            $teacher->remain_licence_count = $teacher->licence_count - 1;
        }else{
            $teacher->remain_licence_count = $teacher->remain_licence_count - 1;
        }
        $teacher->save();

        return redirect('student');
    }
    public function delete_student($id)
    {
        $delete=User::find($id);
        $delete->is_deleted = 1;
        $delete->status = 'deactive';
        $delete->save();
        return redirect('student');
    }
    public function update_student($id)
    {
        $users = User::where('id',$id)->get();
        $data  = [];
        $data['users'] = $users;
        return view('student.update',$data);
    }
    public function updatesave_student($id, Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required',    
        ]);
        if ($validator->fails()) {
            return redirect(route('update_student'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $users = User::find($id);
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = Hash::make($data['password']);
        $users->save();
        return redirect('student');
    }
}
