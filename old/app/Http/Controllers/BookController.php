<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use VIPSoft\Unzip\Unzip;
use App\User;
use App\Category;
use Validator;

use Zip;


class BookController extends Controller
{
    public function book()
    {
        $teacher = Auth::user();
        $list = Book::with('category')->where('is_deleted','0')->orderBy('category_id')->paginate(10);
        // $list = Book::with('category')->join('categories','categories.id','books.category_id')->select('categories.name as catName' )->orderBy('categories.name')->paginate(10);
        // print_r($list);
        // exit;
        $data = [];
        $data['books'] = $list;
        return view('book.list',$data);
    }
    public function serach(Request $request)
    {
        $teacher = Auth::user();
        $serach = $request->get('search');
        $list = Book::with('category')->where('name','like',"%{$serach}%")->where('is_deleted','0')->orderBy('category_id')->paginate(10);
        $data = [];
        $data['books'] = $list;
        return view('book.list',$data);
    }
    public function add_book()
    {
        $category = Category::get();
        $data = [];
        $data['categories'] = $category;
        return view('book.add',$data);
    }
    public function save_book(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'category' => 'required',  
            'book_image' => 'required',  
            'book_upload' => 'required',  
           
        ]);
        if ($validator->fails()) {
            return redirect(route('add_book'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $book = new Book();
        $book->teacher_id = $teacher->id;
        $book->category_id = $data['category'];
        $book->name = $data['name'];
        $book->status = 'active';
        $book->is_deleted = '0';

        $book->book_image = $request->file('book_image')->hashName('');
        $request->file('book_image')
        ->store('image', ['disk' => 'public']);

        $zipNameWithExt = $request->file('book_upload')->getClientOriginalName();
        $zipName = substr($zipNameWithExt, 0, strrpos($zipNameWithExt, '.')); 
        
        $book->book_upload = $request->file('book_upload')->hashName('');
        $request->file('book_upload')
        ->store('book', ['disk' => 'public']);
        $book->save();

        $Path = public_path("/public/book/" . $book->book_upload);
        $extractLocation = public_path("/public/book/" . $book->id);
        $zip = Zip::open($Path);
        $files = $zip->listFiles();
        $zip->extract($extractLocation);
        $zip->close(); 

        $htmlFile = $this->checkForIndexFile($extractLocation, $zipName);
        if($htmlFile){
            $book->index_file_name = $htmlFile;
            $book->save();
        }
        return redirect('book');  
    }
    
    private function checkForIndexFile($extractLocation, $zipName){
        $extrectedFiles = scandir($extractLocation);
        foreach($extrectedFiles as $file){
            if(strpos($file, ".html")){
                return $file;
            }
        }

        $extrectedFiles = scandir($extractLocation ."/" . $zipName);
        foreach($extrectedFiles as $file){
            if(strpos($file, ".html")){
                return $zipName ."/" .$file;
            }
        }
        return "";
    }


    public function book_detail($id)
    {
        $list = Book::where('category_id',$id)->paginate(5);
        $data = [];
        $data['books'] = $list;
        return view('book.detail',$data);
    }
    public function book_view($id)
    {
        $list = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $list;
        return view('book.book-view',$data);
    }
    public function delete_book($id)
    {
        $delete=Book::find($id);
        $delete->is_deleted = '1';
        $delete->save();
        return redirect('book');
    }
    public function book_edit($id)
    {
        $user = Auth::user();
        $category = Category::get();
        $data1 = [];
        $data1['categories'] = $category;
        $edit = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $edit;
        return view('book.edit',$data,$data1);
    }
    public function book_editsave(Request $request,$id)
    {
        // $validator = Validator::make($request->all(), [ 
        //     'name' => 'required', 
        //     'category' => 'required',  
        //     'book_image' => 'required',  
        //     'book_upload' => 'required',  
           
        // ]);
        // if ($validator->fails()) {
        //     return redirect(route('edit_book'))
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }
        $teacher = Auth::user();
        $data = $request->all();
        $book = Book::find($id);
        $book->teacher_id = $teacher->id;
        $book->category_id = $data['category'];
        $book->name = $data['name'];

        if($request->input('book_image'))
        {
            $book->book_image = $request->file('book_image')->hashName('');
            $request->file('book_image')
            ->store('image', ['disk' => 'public']);
            $book->save();
        }

        if($request->input('book_upload'))
        {
            $zipNameWithExt = $request->file('book_upload')->getClientOriginalName();
            $zipName = substr($zipNameWithExt, 0, strrpos($zipNameWithExt, '.')); 
            
            $book->book_upload = $request->file('book_upload')->hashName('');
            $request->file('book_upload')
            ->store('book', ['disk' => 'public']);
            $book->save();

            $Path = public_path("/public/book/" . $book->book_upload);
            $extractLocation = public_path("/public/book/" . $book->id);
            $zip = Zip::open($Path);
            $files = $zip->listFiles();
            $zip->extract($extractLocation);
            $zip->close(); 

            $htmlFile = $this->checkForIndexFile($extractLocation, $zipName);
            if($htmlFile){
                $book->index_file_name = $htmlFile;
                $book->save();
            }
        }

        return redirect('book');
    }
    public function admin_bookview($id)
    {
        $list = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $list;
        return view('book.admin-bookview',$data);
    }
}
