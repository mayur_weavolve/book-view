<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\Category;
use App\User;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
   
    public function category()
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }

        $list=Category::where('is_deleted','0')->orderBy('name')->paginate(10);
        $data=[];
        $data['categories']=$list;
        return view('category.list',$data);
    }
    public function serach(Request $request)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }

        $serach = $request->get('search');
        $list=Category::where('name','like',"%{$serach}%")->where('is_deleted','0')->orderBy('name')->paginate(10);
        $data=[];
        $data['categories']=$list;
        return view('category.list',$data);
    }
    public function add_category()
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        return view('category.add');
    }
    public function save_category(Request $request)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:books', 
            'status' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = new Category();
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];
        $categorys->is_deleted = '0';

        if ($request->file('image')==null){
            $categorys->save();
        }
        else{

            $categorys->image = $request->file('image')->hashName();
            $request->file('image')
            ->store('image',['disk' => 'public']);
            $categorys->save();
        }
        return redirect('category');
    }
    public function delete_category($id)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $delete=Category::find($id);
        $delete->is_deleted = '1';
        $delete->save();
        return redirect('category');
    }
    public function update_category($id)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $categories = Category::where('id',$id)->get();
        $data  = [];
        $data['categories'] = $categories;
        return view('category.update',$data);
    }
    public function updatesave_category($id, Request $request){
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'status' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('update_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = Category::find($id);
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];
        if ($request->file('image')==null){
            $categorys->save();
        }
        else{
            if($categorys->image){
                unlink(public_path('public/image/').$categorys->image);
            }
            $categorys->image = $request->file('image')->hashName();
            $request->file('image')
            ->store('image',['disk' => 'public']);
            $categorys->save();
        }
        return redirect('category');
    }
}
