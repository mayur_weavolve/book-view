<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 

use App\User;
use Validator;

class AdminController extends Controller
{
    public function teacher()
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $list=User::where('role_type','2')
                        ->where('is_deleted',0)
                        ->orderBy('created_at')
                        ->paginate(10);
            $data=[];
            $data['users']=$list;
        }
        return view('teacher.list',$data);

    }
    public function serach(Request $request)
    {
        $user = Auth::user();
        $serach = $request->get('search');
        if($user->role_type == 1)
        {
            if($serach)
            {
                $list=User::where('role_type','2')
                ->where('name','like',"%{$serach}%")
                ->orwhere('email','like',"%{$serach}%")
                ->where('is_deleted',0)
                ->orderBy('created_at')
                ->paginate(10);
                $data=[];
                $data['users']=$list;
            }else{
                $list=User::where('role_type','2')
                ->where('is_deleted',0)
                ->orderBy('created_at')
                ->paginate(10);
                $data=[];
                $data['users']=$list;
            }
        }
        return view('teacher.list',$data);
    }
    public function add_teacher()
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            return view('teacher.add');
        }else{
            return view('404');
        }
    }
    public function save_teacher(Request $request)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|unique:users',  
                'password' => 'required',  
                'licence_count' => 'required',  
                
            ]);
            if ($validator->fails()) {
                return redirect(route('add_teacher'))
                            ->withErrors($validator)
                            ->withInput();
            }
            $data = $request->all();
            $users = new User();
            $users->name = $data['name'];
            $users->email = $data['email'];
            $users->password = Hash::make($data['password']);
            $users->licence_count = $data['licence_count'];
            $users->remain_licence_count = $data['licence_count'];
            $users->status = "active";
            $users->role_type = 2 ;
            $users->is_deleted = 0;
            $users->save();

            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function delete_teacher($id)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $delete=User::find($id);
            $delete->is_deleted = 1;
            $delete->status = 'deactive';
            $delete->save();
            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function update_teacher($id)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $users = User::where('id',$id)->get();
            $data  = [];
            $data['users'] = $users;
            return view('teacher.update',$data);
        }else{
            return view('404');
        }
    }
    public function updatesave_teacher($id, Request $request){
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required',  
                'licence_count' => 'required',   
                
            ]);
            if ($validator->fails()) {
                return redirect(route('update_teacher'))
                            ->withErrors($validator)
                            ->withInput();
            }
            $data = $request->all();
            $user = Auth::user();
            $users = User::find($id);
            $users->name = $data['name'];
            $users->email = $data['email'];
            $diffLicenseCount =  $data['licence_count'] - $users->licence_count;
            $users->licence_count = $data['licence_count'];
            $users->remain_licence_count =  $users->remain_licence_count + $diffLicenseCount ;
            $users->save();
            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function change_password()
    {
        $user = Auth::user();
        return view('change-password');
    }
    public function save_password(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
       
        $validator = Validator::make($request->all(), [ 
            'current_password' => 'required',
            'new_password' => 'required',
            "confirm_password"=> 'required|same:new_password',        
        ]);
        if ($validator->fails()) {
            return redirect(route('change-password'))
                        ->withErrors($validator)
                        ->withInput();
        }
        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return back()->with('error','Your current password does not matches with the password you provided. Please try again.');
        }
        
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return back()->with('error','New Password cannot be same as your current password. Please choose a different password.');
        }
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect('welcome');
    }
    public function profile()
    {
        $user = Auth::user();
        return view('admin_profile');
    }
    public function save_profile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->save();
        return redirect('welcome');
    }
}
