<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'category_id','teacher_id','name', 'book_image', 'status','is_deleted'
    ];
    public function category() {
        return $this->belongsTo(Category::class)->orderBy('name', 'ASC');; 
    }
}
