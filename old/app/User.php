<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
// use Kyslik\ColumnSortable\Sortable;

class User extends Authenticatable
{
    use Notifiable;
    // use Sortable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_type','status','remain_licence_count','licence_count','perent_id',
    ];

    // public $sortable  = [
    //     'name', 'email','remain_licence_count','licence_count'
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function student(){
        return $this->hasOne("App\User");
    }
    public function teacher(){
        return $this->hasOne("App\User", "id", "perent_id");
    }
}
