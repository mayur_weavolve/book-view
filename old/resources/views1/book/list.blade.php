@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Books List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
				@if(Auth::user()->role_type == 1)
					<a href="{{route('add_book')}}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Book </span>
						</span>
					</a>
				@endif
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<!-- <div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('category')}}" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">			
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div> -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Category_id</th>
					<th>Book Name</th>
					<th>Book Image</th>
					<th>Book URL</th>
					<th>Book Status</th>
					@if(Auth::user()->role_type == 1)
					<th>Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
			<?php foreach ($books as $book) { ?>
			<tr>
				<!-- <td> <?php echo $book->id ;?> </td> -->
				<td>{{ $book->category->name }}</td>
				<td>{{ $book->name}}</td>
				<td><img src= "<?php echo URL('/'); ?>/public/image/{{$book->book_image}}" height=50 width=50></td>
				<td><a href = "<?php echo URL('/'); ?>/public/book/{{$book->id}}/{{$book->index_file_name}}">Read Book</td>
				<td>{{ $book->status}}</td>
				@if(Auth::user()->role_type == 1)
				<td>
					<a href="">
						<i class="m-menu__link-icon fa fa-edit"></i>&nbsp;&nbsp;
					<a>
					<a href="{{route('delete_book',[$book->id])}}">
						<i class="m-menu__link-icon fa fa-trash-alt"></i>
					</a>
				</td>
				@endif
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		{{$books->links()}}
	</div>
</div>
</div>
</div>
</div>     
@endsection
