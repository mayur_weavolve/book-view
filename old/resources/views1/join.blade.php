@extends('layouts.app')

@section('content')
                <div class="row">
                    <div class="col-lg-3 col-md-4" style="margin-top:100px">
                        <h2>Your Virtual Lesson</h2>
                        @if($message != "")
                            <div class="alert alert-danger">{{$message}}</div>
                        @endif
                        <form method="post">
                        {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Your Name</label>
                                <input type="text" require class="form-control" placeholder="Your Name" required name="name" id="name">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Lesson ID</label>
                                <input type="text" class="form-control" readonly="readonly" name="team_id" value="{{$data['teamId']}}">
                            </div>
                            <!-- <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="text" class="form-control" placeholder="Password" required name="password" value="">
                            </div> -->
                            <button type="submit" class="btn btn-primary">Join Room</button>
                        </form>
                    </div>
                </div>
@endsection
