@extends('layouts.team')

@section('content')
            <?php if (request()->session()->get('team_created') == "yes") { ?>
                <div class="side-bar teacher">
                <div class="sidebar-handler">
                    <i class="fas fa-bars"></i>
                    <i class="fas fa-times"></i>
                </div> 
            <?php } else { ?>
                <div class="side-bar">
                    <div class="sidebar-handler">
                        <i class="fas fa-arrow-left"></i>
                        <i class="fas fa-arrow-right"></i>
                    </div>
            <?php } ?>

            <div class="side-bar-container">
            
                <?php if (request()->session()->get('team_created') == "no") { ?>
                    <div class="side-bar-part bb1" id="iframeContainer">
                        <a href="" id="download" download target="_blank" >Download</a>
                        <iframe src="" class="spdf" id="song-pdf"></iframe>
                    </div>
                    
                    <?php if (request()->session()->get('room_type') == "Piano" ) { ?>
                        <div class="piano-bar-part" id="generated-chords-student">
                        </div>
                    <?php } else if (request()->session()->get('room_type') == "Guitar" ||  request()->session()->get('room_type') == "Ukulele") { ?>
                        <div class="side-bar-part2 bb1" id="generated-chords-student">
                        </div>
                     <?php } else { ?>
                    
                    <?php } ?>
                <?php } else { ?>
                    <div class="side-bar-part bb1" id="songstrr">
                        <div class="search-box">
                            <input  type="text" class="form-control" placeholder="Search Song" name="saerch" id="search" />
                            <div class="upload-file">
                                Select PDF
                                <input type="file" accept="application/pdf" id="uploadPdf" />
                            </div>
                        </div>
                        <div class="loader"></div>
                        <div class="search-list" id=search-list>
                        </div>
                    </div>

                    <div class="side-bar-part bb1" id="iframeContainer">
                        <button class="goBack"><i class="fas fa-arrow-left"></i></button>
                        <iframe src="" class="spdf" id="song-pdf"></iframe>
                    </div>

                    <?php if (request()->session()->get('room_type') == "Piano") { ?>
                        <div class="piano-bar-part" id="generated-chords">

                            <?php 
                            $pianoChords = [
                                   ["start" => "E0", "endNode" => "A2","hightLight" => "A1,C#1,E1", "name" => "A"  ],
                                   ["start" => "E0", "endNode" => "A2","hightLight" => "A1,C1,E1", "name" => "Am"],

                                   ["start" => "F0", "endNode" => "B2","hightLight" => "B1,D#1,F#1", "name" => "B"],
                                   ["start" => "F0", "endNode" => "B2","hightLight" => "B1,D1,F#1", "name" => "Bm"],

                                   ["start" => "G0", "endNode" => "C2","hightLight" => "C1,E1,G1", "name" => "C"],
                                   ["start" => "G0", "endNode" => "C2","hightLight" => "C1,D#1,G1", "name" => "Cm"],

                                   ["start" => "A0", "endNode" => "D1","hightLight" => "D0,F#0,A1", "name" => "D"],
                                   ["start" => "A0", "endNode" => "D1","hightLight" => "D0,F0,A1", "name" => "Dm"],

                                   ["start" => "B0", "endNode" => "E1","hightLight" => "E0,G#0,B1", "name" => "E"],
                                   ["start" => "B0", "endNode" => "E1","hightLight" => "E0,G0,B1", "name" => "Em"],

                                   ["start" => "C0", "endNode" => "F1","hightLight" => "F0,A1,C1", "name" => "F"],
                                   ["start" => "C0", "endNode" => "F1","hightLight" => "F0,G#0,C1", "name" => "Fm"],

                                   ["start" => "D0", "endNode" => "G1","hightLight" => "G0,B1,D1", "name" => "G"],
                                   ["start" => "D0", "endNode" => "G1","hightLight" => "G0,A#1,D1", "name" => "Gm"],

                            ];
                            foreach( $pianoChords  as $ch){ ?>
                                <div class="piano-ch" data-start="<?php echo $ch['start'] ?>" data-end="<?php echo $ch['endNode'] ?>" data-name="<?php echo $ch['name'] ?>" data-chord="<?php echo $ch['hightLight'] ?>">
                                    <?php echo $ch['name'] ?><piano-keys-sjb startNote="<?php echo $ch['start'] ?>" endNote="<?php echo $ch['endNode'] ?>" highlightedNotes="<?php echo $ch['hightLight'] ?>"></piano-keys-sjb>
                                </div>
                            <?php } ?>
                            
                        </div>
                    <?php } else if (request()->session()->get('room_type') == "Guitar" || request()->session()->get('room_type') == "Ukulele") { ?>
                        <div class="side-bar-part2" id="generated-chords"></div>
                     <?php } ?>
                <?php } ?>
            </div>       
        </div>       
        <div id="player"></div>
        <div class="center-cord" id="center-cord"></div>
        <div class="center-piano-cord" id="center-piano-cord"><img src="" /></div>
@endsection
