<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Book-view | Login</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo URL('/'); ?>/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="<?php echo URL('/'); ?>/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo URL('/'); ?>/theme/css/grayscale.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="">Reading Reading Books</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
       
      </div>
    </div>
  </nav>

  <!-- Header -->
  <header class="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
      
		<div class="m-login__signin">
			<div class="m-login__head">
				<h3 class="m-login__title" style ="color:white">Sign In</h3>
            </div>
            
            <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                    
			<form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
				@csrf
				<div class="form-group m-form__group">
					<input class="form-control m-input" type="text" placeholder="Email" name="email" autocomplete="off">
					@error('email')
								<span class="help-block" role="alert">
									<strong>{{ $errors->first("email") }}</strong>
								</span>
							@enderror
				</div>
				<div class="form-group m-form__group">
					<input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password" name="password">
					@error('password')
							<span class="help-block" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@enderror
				</div>
				<div class="row m-login__form-sub">
					<div class="col m--align-left m-login__form-left">
						<label class="m-checkbox  m-checkbox--light">
							<input type="checkbox" name="remember"> Remember me
							<span></span>
						</label>
					</div>
					<div class="col m--align-right">
						<a href="{{ route('password.request') }}" >Forget Password ?</a>
					</div>
				</div>
				<div class="m-login__form-action">
				<button type="submit" id="m_login_signin_submit" class="view-book" >Sign In</button>
				</div>
			</form>
		</div>
      </div>

    </div>
  </header>

  <footer class="bg-black small text-center text-white-50">
    
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL('/'); ?>/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo URL('/'); ?>/theme/js/grayscale.min.js"></script>

</body>

</html>

