<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Book-view | Reset-Password</title>
  <link href="<?php echo URL('/'); ?>/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/css/grayscale.min.css" rel="stylesheet">

</head>
<body id="page-top">
    <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto login-box text-center">
		      <div class="m-login__signin">
			      <div class="m-login__head">
              <img alt="" src="<?php echo URL('/'); ?>/logo/logo.png" width=300/>
			      </div><br>
			    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
				    @csrf
				    <div class="form-group m-form__group">
              <input type="hidden" name="token" value="{{ $token }}">
              <input id="email" type="email" class="form-control m-input @error('password') is-invalid @enderror" name="email" placeholder="Email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
              @error('email')
                  <span class="invalid-feedback" role="alert">
                      <strong>{{ $message }}</strong>
                  </span>
              @enderror
				    </div>
            <div class="form-group m-form__group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="new-password">
					      @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
				    </div>
            <div class="form-group m-form__group">
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required autocomplete="new-password">
				    </div>
				    <div class="row m-login__form-sub">
					    <div class="col m--align-left m-login__form-left">
						    <label class="m-checkbox  m-checkbox--light">
                  <input type="checkbox" name="remember"> Remember me
                  <span></span>
						    </label>
					    </div>
				    </div>
				    <div class="m-login__form-action">
				      <button type="submit" id="m_login_signin_submit" class="btn btn-primary js-scroll-trigger" style = "background:gray">Submit</button>
				    </div>
		      </form>
		    </div>
      </div>
  </header>
 
  <footer class="text-white">
    <div class = "container footer-bar">
      <div class="">
        2020 Reading Reading Books, LLC
      </div>
      <div class="">
        <a href="">Membership</a>&nbsp;&nbsp;
        <a href="">Terms of Use</a>&nbsp;&nbsp;
        <a href="">Privacy</a>&nbsp;&nbsp;
        <a href="">Help</a>&nbsp;&nbsp;
      </div>
    </div>
  </footer>

  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL('/'); ?>/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/js/grayscale.min.js"></script>

</body>
</html>


