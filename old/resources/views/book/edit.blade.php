@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Edit Book
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('book') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
						<?php foreach ($books as $book) { ?>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Category:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="category" class="form-control m-input" required>
										<option value="">Select</option>
										<?php foreach ($categories as $category) { ?>
										<option value="{{$category->id}}" <?php if($book->category_id == $category->id){ echo 'selected'; } ?> >{{$category->name}}</option>
                                        <?php  } ?>	
																				
									</select>
									@error('category')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('category') }}</strong>
                        				</span>
                    				@enderror
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Book Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="name" class="form-control m-input" placeholder="" value="{{$book->name}}" required>
											@error('name')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('name') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Book Image </label>
									<div class="col-xl-8 col-lg-8">
										<input type="file" name="book_image" class="form-control m-input" placeholder="" value="">
										@if($book->book_image)
										</br><img src="<?php echo URL('/'); ?>/public/image/{{$book->book_image}}" height=100 width=100>
										@endif
										@if ($errors->has('book_image'))
										<span class="helpBlock">
											<strong>{{ $errors->first('book_image') }}</strong>
										</span>
										@endif	
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Book Upload </label>
									<div class="col-xl-8 col-lg-8">
										<input type="file" name="book_upload" class="form-control m-input" placeholder="" value="">
										@if ($errors->has('book_upload'))
										<span class="helpBlock">
											<strong>{{ $errors->first('book_upload') }}</strong>
										</span>
										@endif	
									</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
																
							</div>
							<?php  } ?>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
           
@endsection
