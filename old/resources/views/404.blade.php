@extends('layouts.user')

@section('content')
<div class="container">
    <div class="row justify-content-center" style="margin-top: 100px;">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Unauthorised Request</div>

                <div class="card-body">
                     <h4>You are not authorized to access this page</h4>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
