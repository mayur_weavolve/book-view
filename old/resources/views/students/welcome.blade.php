@extends('layouts.user')
@section('content')
<style>
  .masthead{
    padding: 0rem 0 ;
}
@media only screen and (max-width: 600px) {
    .masthead{
        padding: 0rem 0 !important;
    }
}
</style>
<section class="contact-section ">
<div class="container">
  <div class="row">
    <?php foreach ($categories as $category) { ?>
      <div class="col-md-3 mb-4 ">
        <div class="card py-2 h-100"  >
          <div class="card-body text-center">
          <a href="{{route('book_detail',[$category->id])}}"><img src= "public/image/{{$category->image}}"></a><br><br>
            <!-- <h4 class="text-uppercase m-0"><br>{{$category->name}}</h4> -->
          <div class="small text-black-50">
            <a href="{{route('book_detail',[$category->id])}}" class="btn btn-primary js-scroll-trigger" >Read Books</a>
          </div>
        </div>
      </div>
  </div>
  <?php  } ?>
</div>
</section>
@endsection
