@extends('layouts.admin')
@section('content')
@if(Auth::user()->role_type == 2)
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<div class="row">
					<div class="col-md-6 text-right">			
							<a href = "{{route('book')}}" class ="btn btn-primary">Books</a>
					</div>
                    <div class="col-md-2 text-right">			
							<a href = "{{route('student')}}" class ="btn btn-primary">Students</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endif
</div>
</div>

@endsection