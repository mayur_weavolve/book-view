@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Students
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
				@if(Auth::user()->role_type == 2)
					<a href="{{ route('add_student') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Student </span>
						</span>
					</a>
				@endif
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('student')}}" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search Student"><br>
					</div>
					<div class="col-md-2 text-right">			
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Name</th>
					<th>Username</th>
					<th>Teacher Email</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user) { ?>
			<tr>
				<!-- <td> <?php echo $user->id ;?> </td> -->
				<td>{{ $user->name}}</td>
				<td>{{ $user->email}}</td>
				<td>{{ $user->teacher->email}}</td>
				<td>{{ $user->status}}</td>
				<td>
					<i class="m-menu__link-icon fa fa-edit">
						<a href="{{ route('update_student',[$user->id])}}">Edit</a>
					</i>&nbsp;&nbsp;
				
					<i class="m-menu__link-icon fa fa-trash-alt">
							<a href="{{route('delete_student',[$user->id])}}">Delete</a>
					</i>
				
				</td>
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		{{$users->links()}}
	</div>
</div>
</div>
</div>
</div>     
@endsection
