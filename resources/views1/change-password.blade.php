@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
					Change-Password
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('welcome') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('change-password')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
							<<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Username / Email:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="email" class="form-control m-input" placeholder="" value="{{Auth::user()->email}}" readonly>
									</div>
							</div>
							<div class="form-group m-form__group row{{ $errors->has('current_password') ? ' has-error' : '' }}">
								<label class="col-xl-3 col-lg-3 col-form-label">* Current Password</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input id="current_password" type="password" name="current_password" class="form-control m-input" placeholder="" value="" required>
                                    @if(Session::has('error'))
                                        <span class="help-block" role="alert">
                                            <strong>{{Session::get('error')}}</strong>
                                        </span>
									@endif
                                </div>
							</div>
							<div class="form-group m-form__group row{{ $errors->has('new_password') ? ' has-error' : '' }}">
								<label class="col-xl-3 col-lg-3 col-form-label">* New password:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input id="new_password" type="password" name="new_password" class="form-control m-input" placeholder="" value="" required>
									@if(Session::has('error'))
                                        <span class="help-block" role="alert">
                                            <strong>{{Session::get('error')}}</strong>
                                        </span>
									@endif
                                </div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">*Confirm Password:</label>
                                <div class="col-xl-8 col-lg-8">
                                    <input type="password" name="confirm_password" class="form-control m-input" placeholder="" value="" required>
                                        @error('confirm_password')
                                        <span class="help-block" role="alert">
                                            <strong>{{ $errors->first('confirm_password') }}</strong>
                                        </span>
                                        @enderror
                                </div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
																
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
           
@endsection

