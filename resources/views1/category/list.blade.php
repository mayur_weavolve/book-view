@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Category
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('add_category') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Category Add</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('category')}}" method = "get">
				<div class="row">
					<div class = "col-md-10 text-right">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2 text-right">			
							<button type = "submit" class ="btn btn-primary">Search</button><br>
					</div>
				</div>
				</form>
			</div>
		</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Name</th>
					<th>status</th>
					<th>Image</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($categories as $category) { ?>
			<tr>
				<!-- <td> <?php echo $category->id ;?> </td> -->
				<td>{{ $category->name}}</td>
				<td>{{ $category->status}}</td>
				<td><img src= "public/image/{{$category->image}}" height=50 width=50></td>
					
				
				<td>
					<i class="m-menu__link-icon fa fa-edit">
						<a href="{{ route('update_category',[$category->id])}}">Edit</a>
					</i>&nbsp;&nbsp;
					<i class="m-menu__link-icon fa fa-trash-alt">
							<a href="{{route('delete_category',[$category->id])}}">Delete</a>
					</i>
				</td>
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		{{$categories->links()}}
	</div>
</div>
</div>
</div>
</div>     
@endsection
