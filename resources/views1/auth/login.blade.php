<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Book-view | Login</title>
  <link href="<?php echo URL('/'); ?>/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/css/grayscale.min.css" rel="stylesheet">

</head>
<body id="page-top">
  <!-- <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="">Reading Reading Books</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
      </div>
    </div>
  </nav> -->

  <!-- Header -->
    <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="mx-auto  text-center">
		      <div class="m-login__signin">
			      <div class="m-login__head">
              <img alt="" src="<?php echo URL('/'); ?>/logo/logo.png" width=300/>
			      </div><br>
			    <form class="m-login__form login-box m-form" method="POST" action="{{ route('login') }}">
				    @csrf
				    <div class="form-group m-form__group">
					    <input class="form-control m-input" type="text" placeholder="Username" name="email" required autocomplete="off">
					      @error('email')
                  <span class="help-block" role="alert">
                    <strong>{{ $errors->first("email") }}</strong>
                  </span>
							@enderror
				    </div>
				    <div class="form-group m-form__group">
					    <input class="form-control m-input m-login__form-input--last" type="password" required placeholder="Password" name="password">
					      @error('password')
                  <span class="help-block" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
							  @enderror
				    </div>
				    <div class="row m-login__form-sub">
					    <div class="col m--align-left m-login__form-left">
						    <label class="m-checkbox  m-checkbox--light">
                  <input type="checkbox" name="remember"> Remember me
                  <span></span>
						    </label>
					    </div>
              @if (Route::has('password.request'))
              <div class="col m--align-left m-login__form-left">
                <a href="{{ route('password.request') }}" >Forgot Password?</a>
              </div>
              @endif
				    </div>
				    <div class="m-login__form-action">
				      <button type="submit" id="m_login_signin_submit" class="btn btn-primary js-scroll-trigger" style = "background:gray">Log In</button>
				    </div>
		      </form>
		    </div>
      </div>
  </header>
 
  <footer class="text-white">
    <div class = "container footer-bar">
      <div class="">
        2020 Reading Reading Books, LLC
      </div>
      <div class="">
        <a href="">Membership</a>&nbsp;&nbsp;
        <a href="">Terms of Use</a>&nbsp;&nbsp;
        <a href="">Privacy</a>&nbsp;&nbsp;
        <a href="">Help</a>&nbsp;&nbsp;
      </div>
    </div>
  </footer>

  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL('/'); ?>/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/js/grayscale.min.js"></script>

</body>
</html>
