<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Welcome to Rock Out Loud Live. Your Virtual Music Lesson Portal. </title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="/assets/css/custom.css">
        <link rel="stylesheet" href="/assets/css/all.css">
        @if (config('app.env') === 'production')
            @include('analytics') 
        @endif
    </head>
    <body>
        <div class="flex-center position-ref full-height">
             @yield('content', 'Content Area')
        </div>
    <script>
                <?php if (request()->session()->get('team_created') == "yes") { ?>
                    var redirectUrl = "/";
                <?php } else {  ?>
                     var redirectUrl = "/thank-you";
                <?php } ?>
        var teamId = '{{ request()->session()->get('team_id') }}';
        var userName = '{{ request()->session()->get('name') }}';
        var password = '{{ $team->password }}';
        var roomType = '{{ request()->session()->get('room_type') }}';
        var isStudent = "{{request()->session()->get('team_created')}}" == "no";
        
        <?php if(request()->session()->get('room_type') == "Guitar") { ?>
            var chords = chordLib = [
                    {
                        name: 'E',
                        sequence: '022100 [231]'
                    },
                    {
                        name: 'Em',
                        sequence: '022000 [23]'
                    },
                    {
                        name: 'F',
                        sequence: 'xx3211 [321]'
                    },
                    {
                        name: 'Fm',
                        sequence: 'xx3111 [311]'
                    },
                    {
                        name: 'G',
                        sequence: '320033 [2134]'
                    },
                    {
                        name: 'Gm',
                        sequence: '3x033x [233]'
                    },
                    {
                        name: 'A',
                        sequence: 'x02220 [234]'
                    },
                    {
                        name: 'Am',
                        sequence: 'x02210 [231]'
                    },
                    {
                        name: 'B',
                        sequence: 'x2444x [1333]'
                    },
                    {
                        name: 'Bm',
                        sequence: 'xx4432 [3421]'
                    },
                    {
                        name: 'C',
                        sequence: 'x32010 [321]'
                    },
                    {
                        name: 'Cm',
                        sequence: 'xx5543 [3421]'
                    },
                    {
                        name: 'D',
                        sequence: 'xx0232 [132]'
                    },
                    {
                        name: 'Dm',
                        sequence: 'xx0231 [231]'
                    },
                    {
                        name: 'Maj-Bar',
                        sequence: '133211 [134211]'
                    },
                    {
                        name: 'Min-Bar',
                        sequence: '133111 [134111]'
                    },
                    {
                        name: 'Maj-V',
                        sequence: 'x2444x [134111]'
                    },
                    {
                        name: 'Min-V',
                        sequence: 'x24432 [134121]'
                    },
                    {
                        name: 'Power',
                        sequence: '133xxx [134]'
                    },
                    {
                        name: 'Power',
                        sequence: 'x244xx [134]'
                    }
                ];
                var size=3;
        <?php } else if (request()->session()->get('room_type') == "Ukulele") { ?>
            var size=5;
            var chords = chordLib = [
                    {
                        name: 'A',
                        sequence: '2100 [1234]'
                    },
                    {
                        name: 'Am',
                        sequence: '2000 [1234]'
                    },
                    {
                        name: 'B',
                        sequence: '4322 [1234]'
                    },
                    {
                        name: 'Bm',
                        sequence: '4222 [1234]'
                    },
                    {
                        name: 'C',
                        sequence: '0003 [1234]'
                    },
                    {
                        name: 'Cm',
                        sequence: '0333 [1234]'
                    },
                    {
                        name: 'D',
                        sequence: '2220 [1234]'
                    },
                    {
                        name: 'Dm',
                        sequence: '2210 [1234]'
                    },
                    {
                        name: 'E',
                        sequence: '1402 [1234]'
                    },
                    {
                        name: 'Em',
                        sequence: '0432 [1234]'
                    },
                    {
                        name: 'F',
                        sequence: '2010 [1234]'
                    },
                    {
                        name: 'Fm',
                        sequence: '1013 [1234]'
                    },
                    {
                        name: 'G',
                        sequence: '0232 [1234]'
                    },
                    {
                        name: 'Gm',
                        sequence: '0231 [1234]'
                    }
                ];
        <?php } ?> 
    </script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src='https://j.rockoutloud.live/external_api.js'></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>

    <script src="https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js"></script>

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.14.0/firebase.js"></script>
    <script src='/assets/js/app.js'></script>
    <script src='/assets/js/custom.js'></script>

    <?php if (request()->session()->get('room_type') == "Piano") { ?>
        <script src='/assets/js/piano-lib.js'></script>
    <?php } else if(
        request()->session()->get('room_type') == "Guitar" 
    ||  request()->session()->get('room_type') == "Ukulele") {  ?>   
    <script src='/assets/js/chord-js.js'></script>
    <?php } ?>
    </body>
</html>