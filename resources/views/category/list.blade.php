@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Category
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('category_export') }}" class="btn btn-primary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-download"></i>
						    <span>Export CSV</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item">
					<a href="{{ route('add_category') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New Category Add</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<!-- <div class = "col-md-12">
				<form action = "{{route('category')}}" method = "get">
				<div class="row">
				<div class = "col-md-10">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2">
						<div class="row">
							<div class="col-md-6">			
									<button type = "submit" class ="btn btn-primary">Search</button>
							</div>
							<div class="col-md-6">
							<a href="{{ route('category') }}"  class="btn btn-primary">Clear</a>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div> -->
			@if(sizeof($categories) > 0)
			<div class="col-md-12">
				<span  class="btn btn-primary clear-btn" style="float:right;margin-bottom: 5px;">Clear</span>
			</div>
			@endif
		</div>
		
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Name</th>
					<th>Position</th>
					<th>status</th>
					<th>Image</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($categories as $category) { ?>
			<tr>
				<!-- <td> <?php echo $category->id ;?> </td> -->
				<td>{{ $category->name}}</td>
				<td>{{ $category->position}}</td>
				<td>{{ $category->status}}</td>
				<td><img src= "public/image/{{$category->image}}" alt="{{ $category->name}}"height=50 width=50></td>
					
				
				<td>
					<i class="m-menu__link-icon fa fa-edit">
						<a href="{{ route('update_category',[$category->id])}}">Edit</a>
					</i>&nbsp;&nbsp;
					<i class="m-menu__link-icon fa fa-trash-alt">
							<a href="{{route('delete_category',[$category->id])}}"  onclick="return confirm('Are you sure you want to delete this category?');">Delete</a>
					</i>
				</td>
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		
	</div>
</div>
</div>
</div>
</div> 
<script src="<?php echo URL('/'); ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>  
<script>
				$(document).ready(function() {
					var table = $('#m_table_1').DataTable( {
						lengthChange: false,
						buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
					} );
					$( ".clear-btn" ).click(function() {
						$('.dataTables_filter input').val('');
						table.draw();   
					});
					// table.buttons().container()
					// 	.appendTo( '#example_wrapper .col-md-6:eq(0)' );
					
					$('.dataTables_filter input').unbind().bind('keyup', function() {
						/*var searchTerm = this.value.toLowerCase(),
						regex = '\\b' + searchTerm + '\\b';
						table.rows().search(regex, true, false).draw(); */
						var searchTerm = this.value.toLowerCase()
						if (!searchTerm) {
							table.draw()
							return
						}
						$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
							for (var i=0;i<data.length;i++) {
								if (data[i].toLowerCase().indexOf(searchTerm) >= 0) return true
							}
							return false
						})
						table.draw();   
						$.fn.dataTable.ext.search.pop()
					})
				});
</script>    
@endsection
