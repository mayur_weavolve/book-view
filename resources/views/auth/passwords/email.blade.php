
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Book-view | Reset</title>
  <link href="<?php echo URL('/'); ?>/theme/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <link href="<?php echo URL('/'); ?>/theme/css/grayscale.min.css" rel="stylesheet">

</head>
<body id="page-top">
  <!-- Header -->
    <header class="masthead">
      <div class="container d-flex h-100 align-items-center">
        <div class="align-items-center" style="text-align: center; display: flex; flex-direction: column; width: 100%;">
      <div class="m-login__head">
                    <img alt="RR Book" src="<?php echo URL('/'); ?>/logo/logo.png" width=300/>
          </div><br>
          <div class="m-login__head">
					  <h1 role="heading" aria-level="1">Forgot Password</h1>
				  </div>
        <div class="mx-auto login-box text-center">
		    <div class="m-login__signin">
			   
            @if (session('status'))
                <div class="alert alert-success" style="background: #ffffff;color: #1b4b7b;" role="alert">
                    {{ session('status') }}
                </div>
            @endif
			    <form class="m-login__form m-form" method="POST" action="{{ route('password.email') }}">
				    @csrf
				    <div class="form-group m-form__group">
					    <input class="form-control m-input" aria-label="email" id="email" type="email" placeholder="Username" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
					    @error('email')
                            <span class="help-block" role="alert">
                                <strong>{{ $errors->first("email") }}</strong>
                            </span>
						@enderror
				    </div>
				    <div class="m-login__form-action">
				      <button type="submit" id="m_login_signin_submit" class="btn btn-primary js-scroll-trigger" style = "background:gray">Send Password Reset Link</button>
                      <a href = "{{route('login')}}" class="btn btn-primary js-scroll-trigger" style = "background:gray">Back</a>
				    </div>
		        </form>
		    </div>
        </div>
        </div>
  </header>
 
  <footer class="text-white">
    <div class = "container footer-bar">
      <div class="">
      &copy; 2020 Reading Reading Books, LLC
      </div>
      <div class="">
        <a href="http://booklibrary.alliancetek.org/privacypolicy"  target="_blank" ><b>PRIVACY</b></a>&nbsp;&nbsp;
        <a href="http://booklibrary.alliancetek.org/termsofuse"  target="_blank" >Terms of Use</a>&nbsp;&nbsp;
        <a href="https://www.rrbooks.com/web-subscribe/"  target="_blank" >Subscribe</a>&nbsp;&nbsp;
        <a href="https://www.rrbooks.com/web-contact/"  target="_blank" >Contact</a>&nbsp;&nbsp;
        
      </div>
    </div>
  </footer> 


  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo URL('/'); ?>/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/vendor/jquery-easing/jquery.easing.min.js"></script>

  <script src="<?php echo URL('/'); ?>/theme/js/grayscale.min.js"></script>

</body>
</html>

