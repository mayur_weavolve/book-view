@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Update Teacher
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('teacher') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>

	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
                        <?php foreach ($users as $user) { ?>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Teacher Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="name" aria-label="name" class="form-control m-input" placeholder="" value="{{$user->name}}" required>
											@error('name')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('name') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Teacher Email:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="email" aria-label="email" class="form-control m-input" placeholder="" value="{{$user->email}}" readOnly>
											@error('email')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('email') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Teacher Password:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="password" name="password" aria-label="password" class="form-control m-input" placeholder="" min="8" value="" >
											@error('password')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('password') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Student License:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="licence_count" aria-label="licence_count" class="form-control m-input" placeholder="" value="{{$user->licence_count}}" required>
											@error('licence_count')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('licence_count') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Start Date:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="date" name="start_date" aria-label="start_date" id="start_date" class="form-control m-input" placeholder="" min="<?php echo $user->start_date; ?>" value="<?php echo $user->start_date; ?>">
											@error('start_date')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('start_date') }}</strong> 
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">End Date:</label>
									<div class="col-xl-8 col-lg-8">
								
										<input type="date" name="end_date" aria-label="end_date" id="end_date"  class="form-control m-input" placeholder="" min="<?php echo $user->start_date; ?>" value="<?php echo $user->end_date; ?>">
											@error('end_date')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('end_date') }}</strong>
                        					</span>
                    						@enderror
											<div class="help-block" id="errorDate" ></div>
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">memo:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="memo" aria-label="memo" class="form-control m-input" placeholder="" value="{{$user->memo}}">
											@error('memo')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('memo') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Status:</label>
									<div class="col-xl-8 col-lg-8">
										<select name="status" id="status" class="form-control m-input"aria-label="status">
											<option value="">Select Status</value>
											<option value="active" @if($user->status == 'active') selected @endif>Active</option>
											<option value="deactive" @if($user->status == 'deactive') selected @endif>Inactive</option>
										</select>
											@error('status')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('status') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="submit" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>								
							</div>
                            <?php  } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

   <script type="text/javascript">   

function changeDate(){
	var startDate = new Date($('#start_date').val());
	var endDate = new Date($('#end_date').val());

	if (startDate > endDate){
		$("#errorDate").text('Must select End Date greater than Start Date');
		$('#submit').prop('disabled', true);
	}else{
		$("#errorDate").text('');
		$('#submit').prop('disabled', false);
	}
}
$('#start_date').change(function () {
	changeDate();
});
$('#end_date').change(function () {
	changeDate();
});
</script>            
@endsection
