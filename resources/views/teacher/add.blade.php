@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Add New Teacher
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
				@if(Auth::user()->role_type == 1)
					<a href="{{ route('teacher') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				@endif
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data" method="post" action="{{route('add_teacher')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Teacher Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="name" aria-label="name" class="form-control m-input" placeholder="" value="" required>
											@error('name')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('name') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Teacher Email:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="email" aria-label="email" class="form-control m-input" placeholder="" value="" required>
											@error('email')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('email') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Teacher Password:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="password" name="password" aria-label="password" class="form-control m-input" placeholder="" value="" min="8" required>
											@error('password')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('password') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Student License:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="licence_count" aria-label="licence_count" class="form-control m-input" placeholder="" value="" required>
											@error('licence_count')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('licence_count') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Start Date:</label>
									<div class="col-xl-8 col-lg-8">
										<?php $currentDate = date("Y-m-d"); 
										?>
										<input type="date" name="start_date" aria-label="start_date" id="start_date" class="form-control m-input" placeholder="" min="<?php echo $currentDate; ?>" value="<?php echo $currentDate; ?>">
											@error('start_date')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('start_date') }}</strong> 
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">End Date:</label>
									<div class="col-xl-8 col-lg-8">
										<?php 
											 $dateOneYearAdded = strtotime(date("Y-m-d", strtotime($currentDate)) . " +1 year");
										?>
										<input type="date" name="end_date" aria-label="end_date" id="end_date" class="form-control m-input" placeholder="" min="<?php echo $currentDate; ?>" value="<?php echo date('Y-m-d', $dateOneYearAdded); ?>">
											@error('end_date')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('end_date') }}</strong>
                        					</span>
                    						@enderror
											<div class="help-block" id="errorDate" ></div>
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">Memo:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="memo" aria-label="memo" class="form-control m-input" placeholder="" value="">
											@error('memo')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('memo') }}</strong>
                        					</span>
                    						@enderror
									</div>
							</div>
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="submit" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
																
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>

<script type="text/javascript">   

function changeDate(){
	var startDate = new Date($('#start_date').val());
	var endDate = new Date($('#end_date').val());

	if (startDate > endDate){
		$("#errorDate").text('Must select End Date greater than Start Date');
		$('#submit').prop('disabled', true);
	}else{
		$("#errorDate").text('');
		$('#submit').prop('disabled', false);
	}
}
$('#start_date').change(function () {
	changeDate();
});
$('#end_date').change(function () {
	changeDate();
});
</script>     
@endsection
