<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Book View</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

		<link href="<?php echo URL('/'); ?>/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />
		

		<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
		<link href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.bootstrap4.min.css" rel="stylesheet">
		<link href="<?php echo URL('/'); ?>/assets/css/app.css" rel="stylesheet">
		<!--RTL version:<link href="<?php echo URL('/'); ?>/assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->


		<script>
			var mUtil = {};
				mUtil.isRTL = function(){
						false;
				}
		</script>
		<!--end::Global Theme Styles -->
		
		<!--begin::Page Vendors Styles -->
		<!-- <link href="<?php echo URL('/'); ?>/assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" /> -->

        <!-- Styles -->
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
			.dataTables_paginate a {
				cursor: pointer !important;
			}
			.dataTables_paginate a:hover {
				color: white;
				text-decoration: none;
			}
        </style>
    
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">

			<!-- BEGIN: Header -->
			<header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
				<div class="m-container m-container--fluid m-container--full-height">
					<div class="m-stack m-stack--ver m-stack--desktop">

						<!-- BEGIN: Brand -->
						<div class="m-stack__item m-brand  m-brand--skin-dark ">
							<div class="m-stack m-stack--ver m-stack--general">
								<!-- <div class="m-stack__item m-stack__item--middle m-brand__logo">
									<a href="index.html" class="m-brand__logo-wrapper">
										<img alt="" src="<?php echo URL('/'); ?>/logo/logo.png" height=40 width=170 />
									</a>
								</div> -->
								<div class="m-stack__item m-stack__item--middle m-brand__logo" style="left: 0px;position: absolute;">
									<img alt="" src="<?php echo URL('/'); ?>/logo/logo.png" height="50" width="190" />
								</div>
								<div class="m-stack__item m-stack__item--middle m-brand__tools">

									<!-- BEGIN: Left Aside Minimize Toggle -->
									<a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  " style="top: -8px;">
										<span>a</span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Aside Left Menu Toggler -->
									<a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
										<span>a</span>
									</a>

									<!-- END -->

									<!-- BEGIN: Responsive Header Menu Toggler -->
									<!-- <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler m--visible-tablet-and-mobile-inline-block">
										<span></span>
									</a> -->
									<!-- END -->
									<!-- BEGIN: Topbar Toggler -->
									<a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
										<i class="flaticon-more">a</i>
									</a>
									<!-- BEGIN: Topbar Toggler -->
								</div>
							</div>
						</div>

						<!-- END: Brand -->
						<div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

							<!-- BEGIN: Horizontal Menu -->
							<button type = "reset" class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
							<div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
								<ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
									<li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-link-redirect="1" aria-haspopup="true">
									<!-- <img alt="" src="<?php echo URL('/'); ?>/logo/logo.png" height=50 width=200 /> -->
									</li>
									
								</ul>
							</div>

							<!-- END: Horizontal Menu -->

							<!-- BEGIN: Topbar -->
							<div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
								<div class="m-stack__item m-topbar__nav-wrapper">
									<ul class="m-topbar__nav m-nav m-nav--inline">
										<li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
										 m-dropdown-toggle="click">
											<a href="#" class="m-nav__link m-dropdown__toggle">
												<span class="m-topbar__userpic">
													<img src="<?php echo URL('/'); ?>/assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="user logo" />
												</span>
												<span class="m-topbar__username m--hide">Nick</span>
											</a>
											<div class="m-dropdown__wrapper">
												<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
												<div class="m-dropdown__inner">
													<div class="m-dropdown__header m--align-center" style="background: #4b4b4b">
														<div class="m-card-user m-card-user--skin-dark">
															<div class="m-card-user__pic">
																<img src="<?php echo URL('/'); ?>/assets/app/media/img/users/user4.jpg" class="m--img-rounded m--marginless" alt="user logo" />

																<!--
						<span class="m-type m-type--lg m--bg-danger"><span class="m--font-light">S<span><span>
						-->
															</div>
															<div class="m-card-user__details">
																<span class="m-card-user__name m--font-weight-500" style="color: #ffffff">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
																<a href="123" class="m-card-user__email m--font-weight-300 m-link" style="color: #ffffff">{{ Auth::user()->email }}</a>
															</div>
														</div>
													</div>
													<div class="m-dropdown__body">
														<div class="m-dropdown__content">
															<ul class="m-nav m-nav--skin-light">
																<li class="m-nav__section m--hide">
																	<span class="m-nav__section-text">Section</span>
																</li>
																<li class="m-nav__item">
																	<a href="{{route('admin_profile')}}" class="m-nav__link">
																		<i class="m-nav__link-icon flaticon-profile-1"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">My Profile</span>
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__item">
																	<a href="{{route('change-password')}}" class="m-nav__link">
																		<i class="m-nav__link-icon flaticon-edit"></i>
																		<span class="m-nav__link-title">
																			<span class="m-nav__link-wrap">
																				<span class="m-nav__link-text">Change Password</span>
																				
																			</span>
																		</span>
																	</a>
																</li>
																<li class="m-nav__separator m-nav__separator--fit">
																</li>
																<li class="m-nav__item">
																<a href="{{ route('logout') }}" class="btn m-btn--pill    btn-primary m-btn m-btn--custom m-btn--label-brand m-btn--bolder"
																	onclick="event.preventDefault();
                                                     					document.getElementById('logout-form').submit();">
																		 {{ __('Logout') }} 
																	</a>
																	<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                     										   @csrf
                                    								</form>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>

							<!-- END: Topbar -->
						</div>
					</div>
				</div>
			</header>
			
			<!-- END: Header -->

			<!-- begin::Body -->
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

				<!-- BEGIN: Left Aside -->
				<button type = "reset" class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
				<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

					<!-- BEGIN: Aside Menu -->
					<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
						<ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
							<!-- <li class="m-menu__item  m-menu__item--active" aria-haspopup="true"><a href="{{route('welcome')}}" class="m-menu__link "><i class="m-menu__link-icon flaticon-line-graph"></i><span class="m-menu__link-title"> <span class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											<span class="m-menu__link-badge"></span> </span></span></a></li> -->
							<!-- <li class="m-menu__section ">
								<h4 class="m-menu__section-text">Components</h4>
								<i class="m-menu__section-icon flaticon-more-v2"></i>
							</li> -->
							@if(Auth::user()->role_type == 1)
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="{{route('category')}}" class="m-menu__link"><i class="m-menu__link-icon flaticon-web"></i><span class="m-menu__link-text"><b>Category</b></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
									
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="{{route('teacher')}}" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-web"></i><span class="m-menu__link-text"><b>Teachers</b></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
									
							</li>
							@endif
							@if(Auth::user()->role_type == 1 || Auth::user()->role_type == 2)
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="{{route('student')}}" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-web"></i><span class="m-menu__link-text"><b>Students</b></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
								
							</li>
							<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover"><a href="{{route('book')}}" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon flaticon-web"></i><span class="m-menu__link-text"><b>Books</b></span><i
									 class="m-menu__ver-arrow la la-angle-right"></i></a>
							</li>
							@endif
						</ul>
					</div>

					<!-- END: Aside Menu -->
				</div>

				<!-- END: Left Aside -->
				<div class="m-grid__item m-grid__item--fluid m-wrapper">

					<!-- BEGIN: Subheader -->
					<!-- <div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title ">Dashboard</h3>
							</div>
							
						</div>
					</div> -->

					<!-- END: Subheader -->
					<div class="m-content">
					 		@yield("content")

                    </div>
				
					<!-- begin::Footer -->
					@if(Auth::user()->role_type == 2)
					<footer class="m-grid__item	m-footer">
						<div class = "m-container m-container--fluid m-container--full-height m-page__container">
						<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							
								
								<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
									<ul class="m-footer__nav m-nav m-nav--inline m--pull-center">
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<?php 
												 if(Auth::user()->end_date){
												$OldDate = Auth::user()->end_date;
												$dates = strtotime($OldDate);
												
													$date = date('M d, Y', $dates);
													//print_r($date); exit;
												}
													else{
														$date = '0000-00-00';
													}?>
												<span class="m-nav__link-text">License Expiration Date : <?php  echo $date;?></span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">Number seats remaining : <?php echo Auth::user()->licence_count - count(Auth::user()->student);?> </span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="https://www.rrbooks.com/web-seats/" class="m-nav__link" target="_blank">
												<!-- <span class="m-nav__link-text">Purchase Seats : <?php // echo count(Auth::user()->student);?></span> -->
												<span class="m-nav__link-text">Purchase Seats</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="/termsofuse" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Terms of Use </span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="/privacypolicy" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Privacy</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="https://www.rrbooks.com/web-info/" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Help</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">|</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="https://www.rrbooks.com/web-contact/" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Contact </span>
											</a>
										</li>
										
									</ul>
								</div>
						</div>
					</footer>
 					 @endif
					@if(Auth::user()->role_type == 1)
					<footer class="m-grid__item	m-footer ">
						<div class="m-container m-container--fluid m-container--full-height m-page__container">
							<div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
							
								<div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
									<span class="m-nav__item">
									&copy; 2020 Reading Reading Books, LLC
									</span>
								</div>
							
								<div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
									<ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
										<li class="m-nav__item">
											<a href="" class="m-nav__link">
												<span class="m-nav__link-text">About</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="/privacypolicy" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Privacy</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="/termsofuse" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">T&C</span>
											</a>
										</li>
										<li class="m-nav__item">
											<a href="https://rrbooks.com/web-seats/" class="m-nav__link" target="_blank">
												<span class="m-nav__link-text">Purchase</span>
											</a>
										</li>
										<!-- <li class="m-nav__item m-nav__item">
											<a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
												<i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</footer>
					@endif
					<!-- end::Footer -->
				
		</div>
		
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>


		<script src="<?php echo URL('/'); ?>/assets/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!-- <script src="https://code.jquery.com/jquery-3.5.1.js" /> -->
		<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" />
		<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" />
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js" />
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.bootstrap4.min.js" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js" />
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js" />
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"/>
		<script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.colVis.min.js"/>			
		
	
		<script>
				
		</script>
													
		<!--end:: Global Mandatory Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="<?php echo URL('/'); ?>/assets//demo/base/scripts.bundle.js" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<!-- <script src="<?php echo URL('/'); ?>/assets//vendors/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script> -->

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<!-- <script src="<?php echo URL('/'); ?>/assets//app/js/dashboard.js" type="text/javascript"></script> -->

		<!--end::Page Scripts -->
	</body>

	<!-- end::Body -->
</html>
