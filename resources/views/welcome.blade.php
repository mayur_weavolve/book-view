@extends('layouts.admin')
@section('content')
@if (session('success'))
	<div class="alert alert-success" role="alert">
		{{ session('success') }}
	</div>
@endif
@if(Auth::user()->role_type == 2)
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<div class="row">
					<div class="col-md-6 text-right">			
							<a href = "{{route('book')}}" class ="btn btn-primary">Books</a>
					</div>
                    <div class="col-md-2 text-right">			
							<a href = "{{route('student')}}" class ="btn btn-primary">Students</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endif
</div>
</div>

<!-- The Modal -->
<div class="modal" id="termsModel">
	<div class="modal-dialog">
		<div class="modal-content">

		<!-- Modal Header -->
		<div class="modal-header">
			<h4 class="modal-title">Terms of Use</h4>
			<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
		</div>

		<!-- Modal body -->
		<div class="modal-body">
			I have read and agree to the <a href="/termsofuse" target="_blank">Terms of Use</a> and <a href="/privacypolicy" target="_blank">Privacy Policy</a>.
		</div>

		<!-- Modal footer -->
		<div class="modal-footer">
			<a href="{{ route('accept_terms') }}" type="button" class="btn btn-primary" data-dismiss="modal">Agree</a>
			<a href="{{ route('logout') }}" type="button" class="btn btn-danger dnone" data-dismiss="modal">Close</a>
		</div>

		</div>
	</div>
</div>

<?php if(Auth::user()->is_login == 0){ ?>
	<script type="text/javascript">
			var modal = document.getElementById("termsModel");
			modal.style.display = "block";

			var span = document.getElementsByClassName("close")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}

			var span = document.getElementsByClassName("dnone")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}
	</script>
<?php } ?>

@endsection