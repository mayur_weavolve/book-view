@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Book List
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					@if(Auth::user()->role_type == 1)
						<a href="{{route('book_export')}}" class="btn btn-primary m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
							<span>
								<i class="la la-download"></i>
								<span>Export CSV </span>
							</span>
						</a>
					@endif
				</li>
				<li class="m-portlet__nav-item">
					@if(Auth::user()->role_type == 1)
						<a href="{{route('add_book')}}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
							<span>
								<i class="la la-plus"></i>
								<span>New Book </span>
							</span>
						</a>
					@endif
				</li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<!-- <div class = "col-md-12">
				<form action = "{{route('book')}}" method = "get">
				<div class="row">
					<div class = "col-md-10">
						<input type="search" name="search"class="form-control" placeholder="Search"><br>
					</div>
					<div class="col-md-2">
						<div class="row">
							<div class="col-md-6">			
									<button type = "submit" class ="btn btn-primary">Search</button>
							</div>
							<div class="col-md-6">
							<a href="{{ route('book') }}"  class="btn btn-primary">Clear</a>
							</div>
						</div>
					</div>
				</div>
				</form>
			</div> -->
			@if(sizeof($books) > 0)
			<div class="col-md-12">
				<span  class="btn btn-primary clear-btn" style="float:right;margin-bottom: 5px;">Clear</span>
			</div>
			@endif
		</div>
		<div id="example_wrapper"></div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th> ID</th> -->
					<th>Level</th>
					<!-- <th>Title</th> -->
					<th>Cover</th>
					<!-- <th>Position</th> -->
					<th>Book Image</th>
					<!-- <th>Book Status</th> -->
					@if(Auth::user()->role_type == 1)
					<th colspan="2">Action</th>
					@endif
				</tr>
			</thead>
			<tbody>
			<?php foreach ($books as $book) { ?>
			<tr>
				<!-- <td> <?php echo $book->id ;?> </td> -->
				<td>{{ $book->catName }}</td>
				<td>{{ $book->name}}</td>
				<td><img src= "<?php echo URL('/'); ?>/public/image/{{$book->book_image}}" alt = ""height="50" width="50"></td>
				<!-- <td><a href = "<?php echo URL('/'); ?>/public/book/{{$book->id}}/{{$book->index_file_name}}">Read Book</td> -->
				<!-- <td>{{ $book->position }}</td> -->
				<td><a href="{{ route('admin_bookview',[$book->id])}}" >Read Book</a></td>
				<!-- <td>{{ $book->status}}</td> -->
				@if(Auth::user()->role_type == 1)
				<td>
					<a href="{{route('edit_book',[$book->id])}}" aria-label="link">
						<i class="m-menu__link-icon fa fa-edit"></i>&nbsp;&nbsp;
					</a>
					<a href="{{route('delete_book',[$book->id])}}"  aria-label="link" onclick="return confirm('Are you sure you want to delete this book?');">
						<i class="m-menu__link-icon fa fa-trash-alt"></i>
					</a>
				</td>
				@endif
				</tr>
			<?php } ?>							
			</tbody>
			
		</table>
		
	</div>
</div>
</div>
</div>
</div>
<script src="<?php echo URL('/'); ?>/assets/vendors/datatables/datatables.bundle.js" type="text/javascript"></script>  
<script>
$(document).ready(function() {
					var table = $('#m_table_1').DataTable( {
						lengthChange: false,
						buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
					} );
				
					$( ".clear-btn" ).click(function() {
						$('.dataTables_filter input').val('');
						table.draw();   
					});
					
					$('.dataTables_filter input').unbind().bind('keyup', function() {
						var searchTerm = this.value.toLowerCase()
						if (!searchTerm) {
							table.draw()
							return
						}
						$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
							for (var i=0;i<data.length;i++) {
								if (data[i].toLowerCase().indexOf(searchTerm) >= 0) return true
							}
							return false
						})
						table.draw();   
						$.fn.dataTable.ext.search.pop()
					})
				} );
</script>     
@endsection
