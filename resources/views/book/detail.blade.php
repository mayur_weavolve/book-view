@extends('layouts.user')

@section('content')
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<section class="contact-section ">
<div class="container">
	<div class="row">
	<?php foreach ($books as $book) { ?>
		<div class="col-md-3 mb-4">
			<div class="card py-4 h-100">
				<div class="card-body text-center">
					<a href="{{ route('book_view',[$book->id])}}">
						<img src= "<?php echo URL('/'); ?>/public/image/{{$book->book_image}}" alt="{{$book->name}}">
					</a>
					<h4 class="text-uppercase m-0"><br>{{$book->name}}</h4><br>
					
					<div class="small text-black-50">
						<a href="{{ route('book_view',[$book->id])}}" class="btn btn-primary js-scroll-trigger">Read Book</a>
						<!-- <button type="submit"  class="btn btn-primary js-scroll-trigger" data-toggle="modal" data-target="#myModal">Open Book</button> -->
					</div>
				</div>
			</div>
		</div>
		<?php  } ?>
	</div>
</div>
</section>

@endsection
