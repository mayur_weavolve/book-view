@extends('layouts.user')
@section('content')
<style>
  .masthead{
    padding: 0rem 0 ;
}
@media only screen and (max-width: 600px) {
    .masthead{
        padding: 0rem 0 !important;
    }
}
</style>
<section class="contact-section ">
<div class="container">
  <div class="row">
    <?php foreach ($categories as $category) { ?>
      <div class="col-md-3 mb-4 ">
        <div class="card py-2 h-100"  >
          <div class="card-body text-center">
          <a href="{{route('book_detail',[$category->id])}}"><img src= "public/image/{{$category->image}}" alt="{{$category->name}}"></a><br><br>
            <!-- <h4 class="text-uppercase m-0"><br>{{$category->name}}</h4> -->
          <div class="small text-black-50">
            <a href="{{route('book_detail',[$category->id])}}" class="btn btn-primary js-scroll-trigger" aria-lable = "read book">Read Books</a>
          </div>
        </div>
      </div>
  </div>
  <?php  } ?>
</div>
</section>

<!-- The Modal -->
<div class="modal" id="termsModel">
	<div class="modal-dialog">
		<div class="modal-content">

		<!-- Modal Header -->
		<div class="modal-header">
			<h4 class="modal-title">Terms of Use</h4>
			<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
		</div>

		<!-- Modal body -->
		<div class="modal-body">
			I have read and agree to the <a href="/termsofuse" target="_blank">Terms of Use</a> and <a href="/privacypolicy" target="_blank">Privacy Policy</a>.
		</div>

		<!-- Modal footer -->
		<div class="modal-footer">
			<a href="{{ route('accept_terms') }}" type="button" class="btn btn-primary" data-dismiss="modal">Agree</a>
			<a href="{{ route('logout') }}" type="button" class="btn btn-danger dnone" data-dismiss="modal">Close</a>
		</div>

		</div>
	</div>
</div>

<?php if(Auth::user()->is_login == 0){ ?>
	<script type="text/javascript">
			var modal = document.getElementById("termsModel");
			modal.style.display = "block";

			var span = document.getElementsByClassName("close")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}

			var span = document.getElementsByClassName("dnone")[0];
			span.onclick = function() {
				modal.style.display = "none";
			}
	</script>
<?php } ?>
@endsection
