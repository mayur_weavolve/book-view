
@extends('layouts.user')

@section('content')
<style>
h2{
    text-align: center;
    font-size: 30px !important;
    width: 100%;
    display: inline;
}
.head-div{
    margin-bottom: 20px;
}
.card-body{
    margin-top: 20px;
    margin-bottom: 100px;
}
.navbar{
    background-color: #fff !important;
}
.footer .container{
    padding-top: 10px;
    padding-bottom: 10px; 
}
.footer{
    position: fixed;
    background: #fff;
}

.tc a{
    color: #0000ff;
}
</style>
<div class="contact-section ">
<div class="container">
    <div class="row tc justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <!-- <div class="card-header">Privacy Policy</div> -->

                <div class="card-body">

        <div class = "col-12 head-div">
            <h2 >Privacy Policy </h2>
        </div>
        <div class = "col-12">
            <p>Reading Reading Books, LLC (“RRB”) has created this Privacy Policy in order to demonstrate our commitment to privacy to our customers and users of this website (“Website”). Any RRB customer or user of our Website is sometimes collectively referred to herein as a “Customer”. This Privacy Policy governs the manner in which RRB uses, maintains, and discloses information collected from its Customers, including children who are accessing our Website (and the online digital library accessible through our Website) in order to read books. Children, particularly those under 13, should always check with their parents or guardians to obtain permission before using this Website or entering information on any website or mobile application. We encourage families to discuss their household guidelines regarding the online sharing of personal information.</p>
            <p>As used in this Privacy Policy, the term “Educator” means the school, teacher, tutor, parent, or other adult who purchases a subscription though our affiliated website www.rrbooks.com to access our online digital library (“Library”) located on this Website. </p></br>
            <h3>1. Children’s Online Privacy / COPPA</h3>
            <p>This policy has been developed in accordance with the Children's Online Privacy Protection Act ("COPPA"), among other applicable privacy laws, and outlines our practices in the United States regarding children's information collected through the use of our Website.</p>
            <p>Our products are marketed to adult Educators. We do not allow children to purchase a subscription to our Library. In order for children to access our Library, they must log in using the unique user name and password provided to them by their Educator. We do not require the Educator to provide us with, or in any way disclose to us, any personally identifying information about the child, and we expressly prohibit such disclosure in our <a href = "{{route('termsofuse')}}">Terms and Conditions of Use and Limited License</a>. Accordingly, none of the child’s personally identifying information, such as their full name, address, or date of birth, is connected to their user name when it is assigned and provided to us by their Educator.</p>
            <p>IF YOU ARE UNDER 13, PLEASE GET PERMISSION FROM YOUR PARENT OR LEGAL GUARDIAN BEFORE ACCESSING OUR LIBRARY. ANY PARENT OR LEGAL GUARDIAN WISHING TO DISABLE THEIR CHILD’S ACCESS TO OUR LIBRARY SHOULD CONTACT THE CHILD’S EDUCATOR OR CONTACT RRB AT SERVICE@RRBOOKS.COM.</p>
            
            <h3>2. Information We Collect and Derive</h3>
            <p>The following sections describe the kind of personal data we collect. You can remove your personal data at any time by contacting us at service@rrbooks.com with the subject line “Data Removal Request” and request that your personal data be deleted. Note, however, that certain personal data is required to open and maintain an account with us and to access our Library, as hereinafter described.</p>
            
            <p><i>a. Educator Information Required for Registration</i></p>
            <p>In order to sign up for access to Library, the Educator will be required to provide us with certain personal information, including their full name, job title, school (if applicable), address, and telephone number. We also need to know the number of authorized users (i.e. students) whom the Educator wants to grant access to under the Educator’s Subscription license. Each authorized user must have a user name and password, and we will collect and store such information. Payments for licensing fees may be made by credit card or check. We do not collect or store credit card information that is processed online through our third-party provider. We collect but do not store credit card information for orders placed offline (e.g. over the phone). For orders placed with a purchase order or check, we collect and may store for our records the information that you provide us.</p>
            
            <p><i>b. No Personally Identifying Student Information</i></p>
            <p>We do not have any features on our Website that would allow a child to share any personally identifying information. We do not require or seek to obtain any personally identifying information about any child. Accordingly, we ask that Educators, in signing up for our services, not provide any identifying information of any child, such as a child’s full name, email address, mailing address, or any other information that could be used to identify a child using our services. When choosing user names for each child, Educators should not use a child’s full name, email address, or other personally identifying information. Appropriate user names can be a first name, a random number (e.g. sequential numbers to identify each student), or arbitrary user names.</p>
            
            <p><i>c. Information Collected During Use of Our Website</i></p>
            <p>RRB may collect and maintain automatically collected information from Customers, such as:</br>
            - The name of the domain used to access the Internet or other electronic network activity information, such as your Internet service provider (ISP), browser type and operating system version, language settings, access times, pages viewed, the routes by which you access our Website, and your use of any hyperlinks available within our Website.</br>
            - Device and network identifiers such as IP address.</br>
            - The date and time of each Website visit.</br>
            - The URLs of pages visited and files downloaded from our Website, and related performance and reliability data.</br>
            - Characteristics of devices used to visit our Website, such as operating system, web browser, and screen resolution.</br>
            - Search terms used to find our Website and used while on the Website (in aggregate only, not linked to the Customer).</p>
            <p>Additionally, we may derive information or draw inferences about Customers based on the information we collect. For example, we may make inferences about your location (e.g., country, state, and city) based on your IP address.</p>

            <h3>3. Cookies</h3>
            <p>Our Website may use “cookies” to enhance user experience. A cookie is a piece of data stored on the user’s computer tied to information about the user. They can be session ID cookies and/or persistent cookies. Third parties may also be placing and reading cookies on your browser, or using web beacons to collect information in the course of advertising being serviced on our Website. For session ID cookies, once you close your browser or log out, the cookie terminates and is erased. A persistent cookie is a small text file stored on your computer’s hard drive for an extended period of time. Your browser’s help file contains information and instructions for removing persistent cookies. Session ID cookies may be used to track user preferences while the user is visiting the Website. They also help to minimize load times and save on server processing. Persistent cookies may be used to store whether, for example, you want your password remembered or not, and other information. Users may choose to set their web browser to refuse/disable cookies, or to alert you when cookies are being sent. If you disable cookies, some parts of our Website may not function properly.</p>
            
            <h3>4. Tracking Technology and Log Files</h3>
            <p>RRB does not use log files or tracking technology or collect information about how users access its Website. Because RRB does not use tracking technology, we do not respond to Do Not Track requests.</p>
            
            <h3>5. How We Use and Share the Information We Collect</h3>
            <p>Protecting our Customers’ personal data and providing a safe online environment for children in accordance with COPPA is of utmost importance. While nothing in this Privacy Policy restricts RRB’s use of non-personal information, RRB will not disclose personal information to outside parties, without first obtaining permission from you. RRB will only use personal information for its intended purpose and will not use or share personal information without your express consent except as provided below. </p>
            <ul>
                <li>We will use your personal information to set up your account.</li>
                <li>We will use your personal information to provide and, in some cases, personalize the services.</li>
                <li>We may use personal information to identify and repair errors.</li>
                <li>We may use personal information to contact you for important matters related to your account.</li>
                <li>If you open an account, we will use your personal information to send you information and updates pertaining to your order, to respond to inquiries, and to send you emails and mailings that may include company news, updates, related product and service information, catalogues, etc. If at any time you wish to unsubscribe from receiving future marketing emails, you may follow the unsubscribe instructions at the bottom of each email or contact us at service@rrbooks.com. If you would like to be removed from our postal mailing list, you may contact us at service@rrbooks.com.</li>
                <li>We may disclose personal information to contractors or associates to carry out your requests (for example, to complete a purchase or respond to a troubleshooting request).</li>
                <li>We may disclose personal information if disclosure is required by law or court order or to protect the Website itself.</li>
                <li>We may transfer personal information in connection with the sale, merger, or acquisition of RRB or its assets to the purchaser/acquirer of RRB or its assets.</li>
            
            </ul>

            <h3>6. Protection of Information</h3>
            <p>RRB employs commercially reasonably safeguards and security measures to protect the confidentiality of the information we collect. Unfortunately, no data transmission over the Internet or electronic storage is fully secure. Accordingly, we cannot guarantee that loss, misuse, or alteration of the information collected will not occur despite our best efforts to prevent such occurrences.</p>
            
            <h3>7. Retention and Deletion of Data</h3>
            <p>We will retain your personal information and other data for as long as necessary for the intended purposes in our discretion or as may be required by law. All retained information will remain subject to this Privacy Policy. If you (or we) close your account, or your account becomes inactive, we have no obligation to retain your personal information and other data and may delete such information at any time. Following closure of your account, we may retain your information in our discretion for legitimate business purposes, to prevent fraud or abuse, or as may be required by law. We disclaim any and all liability with respect to retention or deletion of your personal information and other data.</p>
            
            <h3>8. Your Responsibility with Respect to Your Data</h3>
            <p>You are responsible for maintaining the confidentiality of user names and passwords in your possession and control. You are responsible for keeping your data accurate and up-to-date. You can make changes to your data (including user names and passwords) at any time by logging in to your account and editing your profile.</p>
            
            <h3>9. Your California Privacy Rights</h3>
            <p>The California Consumer Privacy Act of 2018 permits our Customers who are California residents the right to: (1) request disclosure of our data collection and sales practices in connection with requesting personal information up to twice in a 12-month period, (2) request a copy of the specific personal information collected about you during the prior 12 months up to twice in a 12-month period, (3) have such information be deleted upon request, (4) to request that your personal information not be sold to third parties, (5) not be discriminated against for exercising any of the foregoing rights. To make any such requests, please send an email to: service@rrbooks.com or write to us at: P.O. Box 6654, Reading, PA 19610 with the subject line: “California Privacy Rights”. In order to verify the identity of the person making a request under this section and be able to respond to it, we will need to collect certain information from the person making such request. We will respond to any request within 45 days of receipt.</p>
            <p>California residents who are under 18 years of age may request and obtain removal of any postings on our Website that they themselves post by emailing us at service@rrbooks.com. Please also note that any requests for removal do not ensure complete or comprehensive removal of the content or information from the Website. For example, content that you or your child have posted may have been republished or reposted by another Customer or third party.</p>
            
            <h3>10. Using the Services Outside the United States</h3>
            <p>RRB is based in the United States, and the information we collect is governed by and operated in accordance with United States law. If you are setting up an account or accessing our Library from outside the United States, you consent to having your information and data transferred to the United States. RRB makes no representation that our Website or data collection and privacy practices are operated in accordance with the laws or regulations of, or governed by, other countries. </p>
            
            <h3>11. Questions</h3>
            <p>Any questions concerning this Privacy Policy may be sent to service@rrbooks.com with the subject line “Privacy Policy Question”. We will do our best to respond to questions promptly.</p>
            
            <h3>12. Changes to Privacy Policy</h3>
            <p>RRB reserves the unilateral right to modify this Privacy Policy, in whole or in part, at any time. Any such changes will take effect immediately when posted on our Website. You should check this page regularly to take notice of any changes we may have made to this Privacy Policy.</p>

            <div> Copyright &copy; 2020 Reading Reading Books, LLC. All Rights Reserved. <br> <br> </div>
            <div>Last updated on September 1, 2020</div>
        </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<style>
    .footer{
        position: fixed;
            background: #fff;
    }
    </style>
@endsection
