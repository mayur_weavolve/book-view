<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //

    protected $fillable = [
        'team_id', 'name' , 'room_type' , 'password'
    ];

}
