<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\Category;
use App\User;
use Validator;
use Intervention\Image\ImageManagerStatic as Image;

class CategoryController extends Controller
{
   
    public function category()
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }

        $list=Category::where('is_deleted','0')->orderBy('position','ASC')->get();
        $data=[];
        $data['categories']=$list;
        return view('category.list',$data);
    }
    public function serach(Request $request)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }

        $serach = $request->get('search');
        $list=Category::where('name','like',"%{$serach}%")->where('is_deleted','0')->orderBy('position','ASC')->get();
        $data=[];
        $data['categories']=$list;
        return view('category.list',$data);
    }
    public function add_category()
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        return view('category.add');
    }
    public function save_category(Request $request)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $validator = Validator::make($request->all(), [ 
            'name' => 'required|unique:categories', 
            'status' => 'required'
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = new Category();
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];
        $categorys->position = $data['position'];
        $categorys->is_deleted = '0';

        if ($request->file('image')==null){
            $categorys->save();
        }
        else{

            $categorys->image = $request->file('image')->hashName();
            $request->file('image')
            ->store('image',['disk' => 'public']);
            $categorys->save();
        }
        return redirect('category');
    }
    public function delete_category($id)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $delete=Category::find($id);
        $delete->delete();
        return redirect('category');
    }
    public function update_category($id)
    {
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $categories = Category::where('id',$id)->get();
        $data  = [];
        $data['categories'] = $categories;
        return view('category.update',$data);
    }
    public function updatesave_category($id, Request $request){
        $user = Auth::user();
        if($user->role_type != 1)
        {
            return view('404');
        }
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'status' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('update_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = Category::find($id);
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];
        $categorys->position = $data['position'];
        if ($request->file('image')==null){
            $categorys->save();
        }
        else{
            if($categorys->image){
                unlink(public_path('public/image/').$categorys->image);
            }
            $categorys->image = $request->file('image')->hashName();
            $request->file('image')
            ->store('image',['disk' => 'public']);
            $categorys->save();
        }
        return redirect('category');
    }

    public function exportCategory()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=categories.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $lists=Category::where('is_deleted','0')->orderBy('name')->get();
        $columns = array('Name', 'Status','Registered Date');

        $callback = function() use ($lists, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($lists as $list) {
                fputcsv($file, array($list->name, $list->status,  $list->created_at));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
