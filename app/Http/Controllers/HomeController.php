<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

use Illuminate\Support\Facades\Auth; 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $list = Category::where('is_deleted','0')->orderBy('position','ASC')->get();
        $data = [];
        $data['categories'] = $list;
      
        if(Auth::user()->role_type == 3) {
            return view('students.welcome',$data);    
        }else{
            return view('welcome',$data);
        }
        
    }
    public function adminhome()
    {
        return view('home');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
