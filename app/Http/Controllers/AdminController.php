<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use App\User;
use Validator;

class AdminController extends Controller
{
    public function teacher()
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $list=User::with('student')->where('role_type','2')
                        ->where('is_deleted',0)
                        ->orderBy('created_at')
                        ->get();
            $data=[];
            $data['users']=$list;
        }
        return view('teacher.list',$data);

    }
    public function serach(Request $request)
    {
        $user = Auth::user();
        $serach = $request->get('search');
        if($user->role_type == 1)
        {
            if($serach)
            {
                $list=User::with('student')->where('role_type','2')
                ->where('name','like',"%{$serach}%")
                ->orwhere('email','like',"%{$serach}%")
                ->where('is_deleted',0)
                ->orderBy('created_at')
                ->get();
                $data=[];
                $data['users']=$list;
            }else{
                $list=User::with('student')->where('role_type','2')
                ->where('is_deleted',0)
                ->orderBy('created_at')
                ->get();
                $data=[];
                $data['users']=$list;
            }
            return view('teacher.list',$data);
        }else{
            return redirect("/");
        }
        
    }
    public function add_teacher()
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            return view('teacher.add');
        }else{
            return view('404');
        }
    }
    public function save_teacher(Request $request)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required|unique:users|email',  
                'password' => 'required|string|min:8', 
                'licence_count' => 'required',  
                
            ]);
            if ($validator->fails()) {
                return redirect(route('add_teacher'))
                            ->withErrors($validator)
                            ->withInput();
            }

            $now = date("Y");
            $date = strtotime($now);
            $new_date = strtotime('+ 1 year', $date);
            
            $data = $request->all();
            $users = new User();
            $users->name = $data['name'];
            $users->email = $data['email'];
            $users->password = Hash::make($data['password']);
            $users->licence_count = $data['licence_count'];
            $users->remain_licence_count = $data['licence_count'];
            // $users->start_date = date("Y-m-d");
            // $users->end_date = date('Y-m-d', $new_date);
            $users->start_date = $data['start_date'];
            $users->end_date = $data['end_date'];
            $users->status = "active";
            $users->role_type = 2 ;
            $users->is_deleted = 0;
            $users->memo = $data['memo'];
            $users->save();

            $credentials = ['email' => $data['email']];
            $response = Password::sendResetLink($credentials, function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function delete_teacher($id)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $delete=User::find($id);
            // $delete->is_deleted = 1;
            // $delete->status = 'deactive';
            $delete->delete();
            
            $studentList = User::with('teacher')->where('role_type',3)->where('is_deleted',0)->where('perent_id',$id)->orderBy('created_at','DESC')->get();
            foreach($studentList as $student){
                $studentObject = User::find($student->id);
                $studentObject->delete();
            }

            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function update_teacher($id)
    {
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $users = User::where('id',$id)->get();
            $data  = [];
            $data['users'] = $users;
            return view('teacher.update',$data);
        }else{
            return view('404');
        }
    }
    public function updatesave_teacher($id, Request $request){
        $user = Auth::user();
        if($user->role_type == 1)
        {
            $validator = Validator::make($request->all(), [ 
                'name' => 'required', 
                'email' => 'required',  
                'licence_count' => 'required',   
                'status' => 'required',  
                'password' => 'nullable|min:8', 
            ]);
            if ($validator->fails()) {
                return redirect(route('update_teacher',$id))
                            ->withErrors($validator)
                            ->withInput();
            }
            $data = $request->all();
            $user = Auth::user();
            $users = User::find($id);
            $oldTeacherStatus =  $users->status;
            $users->name = $data['name'];
            $users->email = $data['email'];
            $diffLicenseCount =  $data['licence_count'] - $users->licence_count;
            $users->licence_count = $data['licence_count'];
            $users->remain_licence_count =  $users->remain_licence_count + $diffLicenseCount ;
            $users->start_date = $data['start_date'];
            $users->end_date = $data['end_date'];
            $users->memo = $data['memo'];
            $users->status =  $data['status'];
            if($data['password'] != ''){
                $users->password = Hash::make($data['password']);
            }
            $users->save();

            if($oldTeacherStatus != $data['status']){
                $studentList = User::with('teacher')->where('role_type',3)->where('is_deleted',0)->where('perent_id',$id)->orderBy('created_at','DESC')->get();
                foreach($studentList as $student){
                    $studentObject = User::find($student->id);
                    $studentObject->status =  $data['status'];
                    $studentObject->save();
                }
            }
            return redirect('teacher');
        }else{
            return view('404');
        }
    }
    public function change_password()
    {
        $user = Auth::user();
        return view('change-password');
    }
    public function save_password(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
       
        $validator = Validator::make($request->all(), [ 
            'current_password' => 'required',
            'new_password' => 'required|string|min:8',
            "confirm_password"=> 'required|same:new_password',        
        ]);
        if ($validator->fails()) {
            return redirect(route('change-password'))
                        ->withErrors($validator)
                        ->withInput();
        }
        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return back()->with('error','Your current password does not match with the password you provided. Please try again.');
        }
        
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return back()->with('error','New Password cannot be same as your current password. Please choose a different password.');
        }
        $user->password = bcrypt($request->get('new_password'));
        $user->save();

        return redirect('welcome')->with('success','Password change successfully.');
    }
    public function profile()
    {
        $user = Auth::user();
        return view('admin_profile');
    }
    public function save_profile(Request $request)
    {
        $user = Auth::user();
        $user->name = $request->get('name');
        $user->save();
        return redirect('welcome');
    }
    public function exportTeacher()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=teachers.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $lists=User::where('role_type','2')->where('is_deleted',0)->orderBy('created_at')->get();
        $columns = array('Name', 'Email', 'Original Licence Count','Remain Licence Count','Start Date','End Date','Memo','Status');

        $callback = function() use ($lists, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($lists as $list) {
                fputcsv($file, array($list->name, $list->email, $list->licence_count, $list->remain_licence_count, $list->start_date, $list->end_date,$list->memo, $list->status));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }

    public function acceptTerms(Request $request)
    {
        $user = Auth::user();
        $user->is_login = 1;
        $user->save();

        $user = Auth::user();
        if($user->role_type == 2){
            return redirect('change-password');
        }else{
            return redirect('welcome');
        }
       
    }
   
}
