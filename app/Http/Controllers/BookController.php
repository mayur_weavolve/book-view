<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use VIPSoft\Unzip\Unzip;
use App\User;
use App\Category;
use Validator;

use Zip;
use DB;

class BookController extends Controller
{
    public function book()
    {
        $teacher = Auth::user();
        $list = DB::select("select books.* , cat.name as catName from books
                    join categories as cat on books.category_id = cat.id
                     ORDER BY books.position");
        $data = [];
        $data['books'] = $list;
        return view('book.list',$data);
    }
    public function serach(Request $request)
    {
        $teacher = Auth::user();
        $serach = $request->get('search');
        $list = Book::with('category')->where('name','like',"%{$serach}%")->where('is_deleted','0')->orderBy('position')->get();


        $list = DB::select("select books.* , cat.name as catName from books
                    join categories as cat on books.category_id = cat.id
                    where books.name LIKE '%$serach%' OR cat.name LIKE '%$serach%' 
                    ORDER BY books.position");

        $data = [];
        $data['books'] = $list;
        return view('book.list',$data);
    }
    public function add_book()
    {
        $category = Category::where("is_deleted", 0)->get();
        $data = [];
        $data['categories'] = $category;
        return view('book.add',$data);
    }
    public function save_book(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'category' => 'required',  
            'book_image' => 'required',  
            'book_upload' => 'required',  
           
        ]);
        if ($validator->fails()) {
            return redirect(route('add_book'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $book = new Book();
        $book->teacher_id = $teacher->id;
        $book->category_id = $data['category'];
        $book->name = $data['name'];
        $book->position = $data['position'];
        $book->status = 'active';
        $book->is_deleted = '0';

        $book->book_image = $request->file('book_image')->hashName('');
        $request->file('book_image')
        ->store('image', ['disk' => 'public']);

        $zipNameWithExt = $request->file('book_upload')->getClientOriginalName();
        $zipName = substr($zipNameWithExt, 0, strrpos($zipNameWithExt, '.')); 
        
        $book->book_upload = $request->file('book_upload')->hashName('');
        $request->file('book_upload')
        ->store('book', ['disk' => 'public']);
        $book->save();

        $Path = public_path("/public/book/" . $book->book_upload);
        $extractLocation = public_path("/public/book/" . $book->id);
        $zip = Zip::open($Path);
        $files = $zip->listFiles();
        $zip->extract($extractLocation);
        $zip->close(); 

        $htmlFile = $this->checkForIndexFile($extractLocation, $zipName);
        if($htmlFile){
            $book->index_file_name = $htmlFile;
            $book->save();
        }
        return redirect('book');  
    }
    
    private function checkForIndexFile($extractLocation, $zipName){
        $extrectedFiles = scandir($extractLocation);
        foreach($extrectedFiles as $file){
            if(strpos($file, ".html")){
                return $file;
            }
        }

        $extrectedFiles = scandir($extractLocation ."/" . $zipName);
        foreach($extrectedFiles as $file){
            if(strpos($file, ".html")){
                return $zipName ."/" .$file;
            }
        }
        return "";
    }


    public function book_detail($id)
    {
        $list = Book::where('category_id',$id)->where('is_deleted','0')->orderBy('position')->get();
        $data = [];
        $data['books'] = $list;
        return view('book.detail',$data);
    }
    public function book_view($id)
    {
        $list = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $list;
        return view('book.book-view',$data);
    }
    public function delete_book($id)
    {
        $delete=Book::find($id);
        $delete->is_deleted = '1';
        $delete->delete();
        return redirect('book');
    }
    public function book_edit($id)
    {
        $user = Auth::user();
        $category = Category::where("is_deleted", 0)->get();
        $data1 = [];
        $data1['categories'] = $category;
        $edit = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $edit;
        return view('book.edit',$data,$data1);
    }
    public function book_editsave(Request $request,$id)
    {
        // $validator = Validator::make($request->all(), [ 
        //     'name' => 'required', 
        //     'category' => 'required',  
        //     'book_image' => 'required',  
        //     'book_upload' => 'required',  
           
        // ]);
        // if ($validator->fails()) {
        //     return redirect(route('edit_book'))
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }
        $teacher = Auth::user();
        $data = $request->all();
        $book = Book::find($id);
        $book->teacher_id = $teacher->id;
        $book->category_id = $data['category'];
        $book->name = $data['name'];
        $book->position = $data['position'];
        if($request->hasFile('book_image'))
        {
            $book->book_image = $request->file('book_image')->hashName('');
            $request->file('book_image')
            ->store('image', ['disk' => 'public']);
        }

        if($request->hasFile('book_upload'))
        {
            $zipNameWithExt = $request->file('book_upload')->getClientOriginalName();
            $zipName = substr($zipNameWithExt, 0, strrpos($zipNameWithExt, '.')); 
            
            $book->book_upload = $request->file('book_upload')->hashName('');
            $request->file('book_upload')
            ->store('book', ['disk' => 'public']);

            $Path = public_path("/public/book/" . $book->book_upload);
            $extractLocation = public_path("/public/book/" . $book->id);
            $zip = Zip::open($Path);
            $files = $zip->listFiles();
            $zip->extract($extractLocation);
            $zip->close(); 

            $htmlFile = $this->checkForIndexFile($extractLocation, $zipName);
            if($htmlFile){
                $book->index_file_name = $htmlFile;
            }
        }
        $book->save();
        return redirect('book');
    }
    public function admin_bookview($id)
    {
        $list = Book::where('id',$id)->get();
        $data = [];
        $data['books'] = $list;
        return view('book.admin-bookview',$data);
    }


    public function exportBook()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=books.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        $lists = DB::select("select books.* , cat.name as catName,us.email as teacherEmail from books
                    join categories as cat on books.category_id = cat.id
                    join users as us on books.teacher_id = us.id
                     ORDER BY books.name");
        $columns = array('Name', 'Category', 'Teacher Email','Status','Registered Date');

        $callback = function() use ($lists, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($lists as $list) {
                fputcsv($file, array($list->name, $list->catName, $list->teacherEmail, $list->status,  $list->created_at));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
