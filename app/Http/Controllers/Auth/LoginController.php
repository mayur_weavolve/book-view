<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Auth\Request;
use Auth;
use Session;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/welcome';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function getCredentials(\Illuminate\Http\Request $request)
    {
        return [
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];
    }

    public function login(\Illuminate\Http\Request $request) {
        $this->validate($request,['email' => 'required','password' => 'required']);
    
        if (Auth::guard()->attempt($this->getCredentials($request))){
            //authentication passed
            $user = Auth::user();
            if($user->status == 'deactive'){
                Auth::logout();
                return redirect(route('login'))->withErrors(['password' => 'Account inactive. Please contact administrator.']);
            }

            $userToken = md5(time() . "_" . rand(10000, 99999));
            $request->session()->put('userLoginToken', $userToken);
            $user->last_session = $userToken;
            $user->save();
        }else{
            Auth::logout();
            return redirect(route('login'))->withErrors(['password' => 'No Match']);
        }
        
        
        return redirect('/');
    }

}
