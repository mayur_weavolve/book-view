<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
class JoinController extends Controller
{
    //

    public function index($teamId, Request $request){

        $team = Team::where('team_id', $teamId)->get()->first();
        if($team){
            $data = [];
            $data['teamId'] = $teamId;
            if($request->session()->get('name') && $teamId ==  $request->session()->get('team_id')){
                $data['name'] = $request->session()->get('name');
                $data['team_created'] = $request->session()->get('team_created');
                return view("team", ["data" =>  $data, "team" => $team]);
            }else{
                return view("join",  ["data" =>  $data , "message" => ""]);
            }
        }else{
            return redirect("/");
        }
    }

    public function joinTeam(Request $request){
        $data = $request->all();
        $team = Team::where('team_id', $data['team_id'])->get()->first();
        $request->session()->put('room_type', $team->room_type);

        if($team){
            $request->session()->put('name', $data['name']);
            $request->session()->put('team_id', $data['team_id']);
            $request->session()->put('team_created', "no");
            return redirect("/join/" . $data['team_id']);
        } else{
            $data['teamId'] = $data['team_id'];
            return view("join",  ["data" =>  $data , "message" => "Please enter valid password"]);
        }
        
    }
    
    public function test(){
        $html = file_get_contents("https://www.songsterr.com/a/wsa/kent-du-ar-anga-tab-s413638t1");
        echo $html = str_replace('<meta charset="utf-8">', '<meta charset="utf-8"><base href="https://songsterr.com/" />' , $html);
    }

    public function uploadFile(Request $request){
        $file = $request->file('file');
        $destinationPath = 'uploads';
        $fileName = time(). "_" . rand(10000, 99999) . ".pdf";
        $file->move($destinationPath, $fileName);
        echo url('/') . "/uploads/" . $fileName;

    }
    public function thankYou(Request $request){
        return view("thank-you");
    }
}
