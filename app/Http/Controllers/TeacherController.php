<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth; 
use App\User;
use Validator;

class TeacherController extends Controller
{
    public function student()
    {
        if(Auth::user()->role_type == 1)
        {
            $list=User::with('teacher')->where('role_type',3)->orderBy('created_at','DESC')->get();
        }
        else{
            $teacher = Auth::user();
            $list=User::with('teacher')->where('role_type',3)->where('is_deleted',0)->where('perent_id',$teacher->id)->orderBy('created_at','DESC')->get();
        }
        $data=[];
        $data['users']=$list;
        return view('student.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        if(Auth::user()->role_type == 1)
        {
            $list=User::with('teacher')
                        ->where('name','like',"%{$serach}%")
                        // ->where('email','like',$serach.'%')
                        ->where('role_type',3)
                        ->orderBy('created_at')
                        ->get();
        }
        else{
            $teacher = Auth::user();
            $list=User::with('teacher')
                        ->where('name','like',"%{$serach}%")
                        // ->where('email','like',$serach.'%')
                        ->where('role_type',3)
                        ->where('is_deleted',0)
                        ->where('perent_id',$teacher->id)
                        ->orderBy('created_at')
                        ->get();
        }
        $data=[];
        $data['users']=$list;
        return view('student.list',$data);
    }
    public function add_student()
    {
        $teacher = Auth::user();
        $teacher = User::with('teacher')->find($teacher->id);
        if($teacher->licence_count <= count($teacher->student)){
            return redirect(route('student')."?error=limit");
        }

        return view('student.add');
    }
    public function save_student(Request $request)
    {

        $messages = array(
            'email.required' => 'This username or email field is required',
            'email.unique' => 'Username taken',
            'password.required' => 'This password field is required',
            'confirm_student_password.same' => 'Passwords do not match',
        );

        $rules = array(
            'email' => 'required|unique:users',  
            'password' => 'required|string|min:8',  
            'confirm_student_password' => 'required|string|same:password|min:8',  
            
        );
       
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            return redirect(route('add_student'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $teacher = Auth::user();
        $data = $request->all();
        $users = new User();
        $users->name = $data['name'];
        $users->email = $data['email'];
        $users->password = Hash::make($data['password']);
        $users->perent_id = $teacher->id;
        $users->status = "active";
        $users->role_type = 3 ;
        $users->is_deleted = 0;
        $users->save();
        if($teacher->remain_licence_count == ''){
            $teacher->remain_licence_count = $teacher->licence_count - 1;
        }else{
            $teacher->remain_licence_count = $teacher->remain_licence_count - 1;
        }
        $teacher->save();

        return redirect('student');
    }
    public function delete_student($id)
    {
        $teacher = Auth::user();
        $users = User::find($teacher->id);
        $users->remain_licence_count =  $users->remain_licence_count + 1 ;
        $users->save();

            
        $delete=User::find($id);
        // $delete->is_deleted = 1;
        // $delete->status = 'deactive';
        $delete->delete();
        return redirect('student');
    }
    public function update_student($id)
    {
        $users = User::where('id',$id)->get();
        $data  = [];
        $data['users'] = $users;
        return view('student.update',$data);
    }
    public function updatesave_student($id, Request $request){
        $messages = array(
            'password.required' => 'This password field is required',
            'confirm_student_password.same' => 'Passwords do not match',
        );

        $validator = Validator::make($request->all(), [ 
            'email' => 'required',   
            'password' => 'nullable|min:8',
            'confirm_student_password' => 'nullable|same:password|min:8',      
        ], $messages);

        if ($validator->fails()) {
            return redirect(route('update_student',$id))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $users = User::find($id);
        $users->name = $data['name'];
        $users->email = $data['email'];
        if($data['password'] != ''){
            $users->password = Hash::make($data['password']);
        }
        $users->save();
        return redirect('student');
    }

    public function exportStudent()
    {
        $headers = array(
            "Content-type" => "text/csv",
            "Content-Disposition" => "attachment; filename=students.csv",
            "Pragma" => "no-cache",
            "Cache-Control" => "must-revalidate, post-check=0, pre-check=0",
            "Expires" => "0"
        );
        if(Auth::user()->role_type == 1)
        {
            $lists=User::with('teacher')->where('role_type',3)->where('is_deleted',0)->orderBy('created_at','DESC')->get();
        }
        else{
            $teacher = Auth::user();
            $lists=User::with('teacher')->where('role_type',3)->where('is_deleted',0)->where('perent_id',$teacher->id)->orderBy('created_at','DESC')->get();
        }
        $columns = array('Name', 'Email', 'Teacher Email','Status','Registered Date');

        $callback = function() use ($lists, $columns)
        {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach($lists as $list) {
                fputcsv($file, array($list->name, $list->email, $list->teacher->email, $list->status,  $list->created_at));
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }
}
