<?php

namespace App\Http\Controllers;

class OpenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function privacypolicy()
    {
        return view('privacypolicy');
    }

    public function termsofuse()
    {
        return view('termsofuse');
    }
}
